#include "user_interface.h"

#include "assert.h"
#include "stdlib.h"
#include "string.h"
#include "stdbool.h"
#include "util_string.h"

static inline void UsbCallbackInit(UsbUserInterface_t* pUsb);
void SendToDevice(void* pHandle, char* pData, size_t Size);
void SendToPn6280(void* pHandle, char* pData, size_t Size);
void TransmitMsg(void* pHandle, const uint8_t* pData, size_t Size);

UsbUserInterface_t* UsbUserInterfaceInit(USBD_HandleTypeDef* pUsbHandle, UserInterfaceCallback_t* pExternCallbacks)
{
	assert(pUsbHandle);

	UsbUserInterface_t* pUsbUser = (UsbUserInterface_t*)fpUtilMalloc(sizeof(UsbUserInterface_t));
	pUsbUser->pInterface = (UsbInterfaceSoftware_t*)fpUtilMalloc(sizeof(UsbInterfaceSoftware_t));
	pUsbUser->pInterface->pSoftUsb = (UsbPortSoftware_t*)fpUtilMalloc(sizeof(UsbPortSoftware_t));
	UsbPort_t* pPort = (UsbPort_t*)fpUtilMalloc(sizeof(UsbPort_t));
	UsbPortInit(pPort, pUsbHandle);
	UsbPortSoftInit(pUsbUser->pInterface->pSoftUsb, pPort, 100);

	UsbRingBufferInit();
	pUsbUser->pProtocol = UsbUserProtocolInit(pUsbUser, (char*)UsbBuffer.pBegin, (char*)UsbBuffer.pEnd, (char*)UsbBuffer.pCursor);
	pUsbUser->pCallback = pExternCallbacks;
	UsbCallbackInit(pUsbUser);

	return pUsbUser;
}

static inline void UsbCallbackInit(UsbUserInterface_t* pUsb)
{
	UsbUserProtocol_t* pProtocol = (UsbUserProtocol_t*)pUsb->pProtocol;

	pProtocol->Callbacks->fpForward = SendToPn6280;
	pProtocol->Callbacks->fpTransmit = TransmitMsg;
}

void UsbUserInterfaceDeinit(UsbUserInterface_t* pUsbInterface)
{
	assert(pUsbInterface);

	UsbUserProtocolDeinit(pUsbInterface->pProtocol);
	UsbRingBufferDeinit();

	fpUtilFree(pUsbInterface->pInterface->pSoftUsb);
	fpUtilFree(pUsbInterface->pInterface);
	fpUtilFree(pUsbInterface);
}

void UserInterfaceProcess(UsbUserInterface_t* pUsbInterface)
{
	assert(pUsbInterface);
	UsbUserProtocol_t* pProtocol = (UsbUserProtocol_t*)pUsbInterface->pProtocol;
	static char* pLastCursor = NULL;

	if (pLastCursor != (char*)UsbBuffer.pCursor)
	{
		pProtocol->Parser.pWrite = (char*)UsbBuffer.pCursor;
		pLastCursor = (char*)UsbBuffer.pCursor;
		UsbUserProtocolProcess(pProtocol);
	}
}

void SendToPn6280(void* pHandle, char* pData, size_t Size)
{
	UsbUserInterface_t* pUsbUser = (UsbUserInterface_t*)pHandle;
    pUsbUser->pCallback->fpSendToPn6280(pData, Size);
}

void SendToDevice(void* pHandle, char* pData, size_t Size)
{
	UsbUserInterface_t* pUsbUser = (UsbUserInterface_t*)pHandle;
  pUsbUser->pCallback->fpSendToDevice(pData, Size);

}

void TransmitMsg(void* pHandle, const uint8_t* pData, size_t Size)
{
	UsbUserInterface_t* pUsb = (UsbUserInterface_t*)pHandle;
	char* pMsg = (char*)fpUtilMalloc(Size);
	memcpy(pMsg, pData, Size);
	UsbSoftTransmit(pUsb->pInterface, pMsg, Size, false);
}


