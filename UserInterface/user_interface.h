#ifndef USER_INTERFACE_H_
#define USER_INTERFACE_H_

#include "ui_protocol.h"

extern UsbBuffer_t UsbBuffer;
extern USBD_HandleTypeDef hUsbDeviceFS;

typedef void (*TransmitDevice)(char* pMsg, size_t Size);
typedef void (*TransmitePn6280)(char* pMsg, size_t Size);

typedef struct
{
	TransmitDevice fpSendToDevice;
	TransmitePn6280 fpSendToPn6280;
} UserInterfaceCallback_t;

typedef struct
{
	void* pProtocol;
	UsbInterfaceSoftware_t*  pInterface;
	UserInterfaceCallback_t* pCallback;
} UsbUserInterface_t;

UsbUserInterface_t* UsbUserInterfaceInit(USBD_HandleTypeDef* pUsbHandle, UserInterfaceCallback_t* pExternCallbacks);
void UsbUserInterfaceDeinit(UsbUserInterface_t* pUsbInterface);
void UserInterfaceProcess(UsbUserInterface_t* pUsbInterface);
void TransmitMsg(void* pHandle, const uint8_t* pData, size_t Size);

#endif /* USER_INTERFACE_H_ */
