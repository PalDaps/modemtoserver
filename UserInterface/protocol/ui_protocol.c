#include "ui_protocol.h"
#include "util_string.h"
#include "stdlib.h"

UsbUserProtocol_t* UsbUserProtocolInit(void* pHandle, char* pBegin, char* pEnd, char* pWrite)
{
	  assert(pBegin);
	  assert(pEnd);
	  assert(pWrite);


	  UsbUserProtocol_t* pProtocol = malloc(sizeof(UsbUserProtocol_t));
	  RingBuffer_t* pRingBuff = malloc(sizeof(RingBuffer_t));
	  UsbCallbacks_t* pCallbacks = malloc(sizeof(UsbCallbacks_t));

	  pRingBuff->pBegin = pBegin;
	  pRingBuff->pEnd = pEnd;
	  pProtocol->Parser.pRingBuffer = pRingBuff;
	  pProtocol->Parser.pWrite = pWrite;
	  pProtocol->Parser.pRead = pBegin;
	  pProtocol->Callbacks = pCallbacks;
	  pProtocol->pHandle = pHandle;
	  return pProtocol;
}

void UsbUserProtocolDeinit(UsbUserProtocol_t* pProtocol)
{
  assert(pProtocol);

  free(pProtocol->Callbacks);
  free(pProtocol->Parser.pRingBuffer);
  free(pProtocol);
}

void UsbUserProtocolProcess(UsbUserProtocol_t* pProtocol)
{
	assert(pProtocol);

	UsbParser_t* pParser = &pProtocol->Parser;
	const RingBuffer_t* pRingBuffer = pParser->pRingBuffer;
	String_t Str = StrMakeFromRingBuffer(pRingBuffer, pParser->pRead, pParser->pWrite);

	if (Str.Length)
		pProtocol->Callbacks->fpForward(pProtocol->pHandle, Str.pData, Str.Length);

	StrClear(&Str);
	pParser->pRead = pParser->pWrite;
}
