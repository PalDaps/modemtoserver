#ifndef UI_PROTOCOL_H_
#define UI_PROTOCOL_H_

#include "usb_driver_stm.h"
#include "usb_driver.h"
#include "usb_driver_stm_ex.h"
#include "stm32f4xx_hal.h"
#include "util_ring_buffer.h"
#include "usb_driver_software.h"
#include "usb_driver_freertos.h"
#include "util_heap.h"

typedef void (*TransmitToDevice)(void* pHandle, char* pData, size_t Size);
typedef void (*Transmit)(void* pHandle, const uint8_t* pData, size_t Size);

typedef struct
{
	TransmitToDevice fpForward;
	Transmit fpTransmit;
} UsbCallbacks_t;

typedef struct
{
	RingBuffer_t* pRingBuffer;
	char* pWrite;
	char* pRead;
} UsbParser_t;

typedef struct
{
	UsbParser_t Parser;
	UsbCallbacks_t* Callbacks;
	void* pHandle;
} UsbUserProtocol_t;

UsbUserProtocol_t* UsbUserProtocolInit(void* pHandle, char* pBegin, char* pEnd, char* pWrite);
void UsbUserProtocolDeinit(UsbUserProtocol_t* pProtocol);
void UsbUserProtocolProcess(UsbUserProtocol_t* pProtocol);

#endif /* UI_PROTOCOL_H_ */
