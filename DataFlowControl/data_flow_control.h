#ifndef DATA_FLOW_CONTROL_H_
#define DATA_FLOW_CONTROL_H_

#include "string.h"
#include "cmsis_os.h"
#include "stdbool.h"
#include "general_types.h"

typedef enum
{
  CONNECT_PROTOCOL_UNDEF = 0,
  CONNECT_PROTOCOL_NO,
  CONNECT_PROTOCOL_JSON_PROGRESS,
  CONNECT_PROTOCOL_JSON,
  CONNECT_PROTOCOL_EGTS,
  CONNECT_PROTOCOL_NTRIP,
  CONNECT_PROTOCOL_NTP
} ConnectPointProtocol_t;

typedef enum
{
  SOCKET_STATUS_UNDEF = 0,
  SOCKET_STATUS_CLOSE,
  SOCKET_STATUS_GSM_WAIT_OPEN,
  SOCKET_STATUS_GSM_OPEN,
  SOCKET_STATUS_GSM_SEND_CLOSE,
  SOCKET_STATUS_GSM_WAIT_AUTH,
  SOCKET_STATUS_GSM_WAIT_DATA,
  SOCKET_STATUS_GSM_WAIT_CLOSE,
  SOCKET_STATUS_GSM_AUTH,
  SOCKET_STATUS_ETH_WAIT_OPENING,
  SOCKET_STATUS_ETH_OPEN,
  SOCKET_STATUS_ETH_WAIT_AUTH,
  SOCKET_STATUS_ETH_AUTH
} SocketState_t;

typedef enum
{
  SEND_STATE_UNDEF = 0,
  SEND_STATE_ATCMD,
  SEND_STATE_WAIT_CMD,
  SEND_STATE_DATA,
  SEND_STATE_WAIT_DATA,
}SendState_t;

typedef struct
{
  char Addr[64];
  uint16_t Port;
  ConnectPointProtocol_t Protocol;
  bool Auth;
  uint8_t SockNumber;
  SocketState_t Status;
  SendState_t SendState;
  char Mount[5];
  char User[33];
  char Pass[33];
  uint16_t Timeout;
}CommonConnectPoint_t;

typedef struct
{
	Id_t Id;
	CommonConnectPoint_t CmdPoint;
}DataFlowControl_t;

DataFlowControl_t* DataFlowControlInit();

#endif /* DATA_FLOW_CONTROL_H_ */
