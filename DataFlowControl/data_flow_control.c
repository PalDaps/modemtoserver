#include "data_flow_control.h"
#include "util_heap.h"

DataFlowControl_t* DataFlowControlInit()
{
	DataFlowControl_t* pDataFlowControl = (DataFlowControl_t*)fpUtilMalloc(sizeof(DataFlowControl_t));

	memset(&pDataFlowControl->Id, 0x00, sizeof(pDataFlowControl->Id));

	pDataFlowControl->CmdPoint.SockNumber = 0;
  memcpy(pDataFlowControl->CmdPoint.Addr, "194.58.79.41", 12);
  pDataFlowControl->CmdPoint.Port = 28344;
  pDataFlowControl->CmdPoint.Protocol = CONNECT_PROTOCOL_JSON_PROGRESS;
  pDataFlowControl->CmdPoint.Auth = true;

  return pDataFlowControl;
}
