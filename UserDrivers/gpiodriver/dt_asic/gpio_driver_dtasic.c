#if defined(DT_ASIC)

/**
 * @addtogroup GpioDriverDtAsic
 * @{
 */


#include "assert.h"
#include "stdbool.h"
#include "gpio_driver_dtasic.h"

/**
 * @brief Инициализация струкутры
 * @param[out] pGpio GPIO
 * @param[in]  pPort Порт
 * @param[in]  Pin   Пин
 */
void GpioInit(Gpio_t* pGpio, GPIO_TypeDef* pPort, uint16_t Pin)
{
  assert(pGpio);

  pGpio->pPort = pPort;
  pGpio->Pin = Pin;
   pGpio->pPort->Dir = (GPIO_PIN_All & (~(GPIO_PIN_1 | GPIO_PIN_15)));
  
  pGpio->pPort->EventEn |= GPIO_PIN_1;
		
  while (pGpio->pPort->EventState != 0) 
    pGpio->pPort->EventState = GPIO_PIN_1;	
		
  pGpio->pPort->EventMask |= GPIO_PIN_1;
  // Выключение внешнего уилителя (по-умолчанию)
//  pGpio->Pin = GPIO_PIN_15;
//  GpioSetValue(pGpio, 1); 
}

/**
 * @brief Деинициализация струкутры
 * @param[in] pGpio GPIO
 */
void GpioDeinit(Gpio_t* pGpio)
{
  assert(pGpio);

  pGpio->pPort = NULL;
  pGpio->Pin = -1;
}

/**
 * @brief Установка значения на пине
 * @param[in] pGpio GPIO
 * @param[in] Value Состояние пина
 */
void GpioSetValue(const Gpio_t* pGpio, bool Value)
{
  assert(pGpio);  
  Value ? (pGpio->pPort->Set |= pGpio->Pin) : (pGpio->pPort->Reset |= pGpio->Pin);
}

/**
 * @brief Получение значения с пина
 * @param[in] pGpio GPIO
 * @return Состояние пина
 */
bool GpioGetValue(const Gpio_t* pGpio)
{
  assert(pGpio);  
  return (pGpio->pPort->State & pGpio->Pin) == pGpio->Pin;
}

/**
 * @brief Инвертирование состояния пина
 * @param[in] pGpio GPIO
 */
void GpioToggle(const Gpio_t* pGpio)
{
  return;
}

/**
 * @brief Устанавливает направление выводов
 * @param[in] pGpio GPIO
 * @param[in] Direct направление выводов GPIO
 * @param[in] Pins пины для конфигурации
 */
void GpioSetDirection(Gpio_t* pGpio, uint8_t Direct, uint16_t Mask)
{
  assert(pGpio);
  (Direct == 0) ? (pGpio->pPort->Dir &= ~Mask) : (pGpio->pPort->Dir |= Mask);
}

/// @}

#endif /* DT_ASIC */
