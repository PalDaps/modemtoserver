#if defined(DT_ASIC)
/**
 * @file
 * @brief Заголовочный файл с описанием API GPIO для DT ASIC
 */
#ifndef GPIO_DRIVER_DTASIC_H_
#define GPIO_DRIVER_DTASIC_H_

/**
 * @addtogroup GpioDriver
 * @{
 */
 
/**
 * @defgroup GpioDriverDtAsic
 * @brief Реализация драйвера Gpio для DT ASIC
 * @{
 */
//#include "base_address.h"
#include <stdint.h>
#include <stddef.h>
#include "gpio_driver.h"

#define GPIO_PIN_0                  ((uint16_t)0x0001)  ///< Выбран Pin 0
#define GPIO_PIN_1                  ((uint16_t)0x0002)  ///< Выбран Pin 1
#define GPIO_PIN_2                  ((uint16_t)0x0004)  ///< Выбран Pin 2 
#define GPIO_PIN_3                  ((uint16_t)0x0008)  ///< Выбран Pin 3
#define GPIO_PIN_4                  ((uint16_t)0x0010)  ///< Выбран Pin 4 
#define GPIO_PIN_5                  ((uint16_t)0x0020)  ///< Выбран Pin 5
#define GPIO_PIN_6                  ((uint16_t)0x0040)  ///< Выбран Pin 6
#define GPIO_PIN_7                  ((uint16_t)0x0080)  ///< Выбран Pin 7
#define GPIO_PIN_8                  ((uint16_t)0x0100)  ///< Выбран Pin 8
#define GPIO_PIN_9                  ((uint16_t)0x0200)  ///< Выбран Pin 9
#define GPIO_PIN_10                 ((uint16_t)0x0400)  ///< Выбран Pin 10
#define GPIO_PIN_11                 ((uint16_t)0x0800)  ///< Выбран Pin 11
#define GPIO_PIN_12                 ((uint16_t)0x1000)  ///< Выбран Pin 12
#define GPIO_PIN_13                 ((uint16_t)0x2000)  ///< Выбран Pin 13
#define GPIO_PIN_14                 ((uint16_t)0x4000)  ///< Выбран Pin 14
#define GPIO_PIN_15                 ((uint16_t)0x8000)  ///< Выбран Pin 15
#define GPIO_PIN_All                ((uint16_t)0xFFFF)  ///< Выбраны все Pin

/// @brief Регистры GPIO
typedef struct
{
  volatile uint32_t Dir;         ///< Address offset: 0x00 | Направление.
  volatile uint32_t Alt;         ///< Address offset: 0x04 | Выбор основной альтернативной функции.
  volatile uint32_t Set;         ///< Address offset: 0x08 | Установка в "1".
  volatile uint32_t Reset;       ///< Address offset: 0x0C | Установка в "0".
  volatile uint32_t State;       ///< Address offset: 0x10 | Входное значение.
  volatile uint32_t EventEn;     ///< Address offset: 0x14 | Разрешение детектирования событий.
  volatile uint32_t Front;       ///< Address offset: 0x18 | Тип фронта.
  volatile uint32_t EventState;  ///< Address offset: 0x1C | Состояние событий.
  volatile uint32_t EventMask;   ///< Address offset: 0x20 | Маска событий.
} GPIO_TypeDef;

/// @brief Обертка над GPIO
typedef struct Gpio_
{
  GPIO_TypeDef* pPort;            ///< Порт
  uint16_t      Pin;              ///< Пин
} Gpio_t;

void GpioInit(Gpio_t* pGpio, GPIO_TypeDef* pPort, uint16_t Pin);
void GpioDeinit(Gpio_t* pGpio);
void GpioSetDirection(Gpio_t* pGpio, uint8_t Direct, uint16_t Mask);

/// @}

/// @}

#endif /* GPIO_DRIVER_DTASIC_H_ */

#endif /* DT_ASIC */
