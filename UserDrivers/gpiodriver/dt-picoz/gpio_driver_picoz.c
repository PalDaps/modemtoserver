#if defined(DT_Z7030)

/**
 * @addtogroup GpioDriverPicoZ
 * @{
 */

#include "assert.h"
#include "stdbool.h"
#include "gpio_driver_picoz.h"

/**
 * @brief Инициализация струкутры
 * @details Gpio_t хранит указатель на порт GPIO.
 * Поэтому порт GPIO должен быть сконструирован и деконструирован извне
 * @param[out] pGpio Структура с параметрами драйвера GPIO
 * @param[in]  pPort Порт в PS ПЛИС
 * @param[in]  Channel Канал интерфейса (1 или 2)
 * @param[in]  Pin   Пин (0 .. 31)
 */
void GpioInit(Gpio_t* pGpio, XGpio* pPort, unsigned Channel, uint8_t Pin)
{
  assert(pGpio);

  pGpio->pPort = pPort;
  pGpio->Channel = Channel;
  pGpio->Mask = (0x1 << Pin);
  pGpio->Pin = Pin;
}

/**
 * @brief Деинициализация струкутры
 * @param[in] pGpio Структура с параметрами драйвера GPIO
 */
void GpioDeinit(Gpio_t* pGpio)
{
  assert(pGpio);

  pGpio->pPort = NULL;
  pGpio->Mask = (u32)0;
}

/**
 * @brief Установка значения на пине
 * @param[in] pGpio Структура с параметрами драйвера GPIO
 * @param[in] Value Состояние пина
 */
void GpioSetValue(const Gpio_t* pGpio, bool Value)
{
  assert(pGpio);

  if (Value)
    XGpio_DiscreteSet(pGpio->pPort, pGpio->Channel, pGpio->Mask);
  else
    XGpio_DiscreteClear(pGpio->pPort, pGpio->Channel, pGpio->Mask);
}

/**
 * @brief Чтение значения на пине
 * @param[in] pGpio Структура с параметрами драйвера GPIO
 * @return Состояние пина
 */
bool GpioGetValue(const Gpio_t* pGpio)
{
  assert(pGpio);

  return ((XGpio_DiscreteRead(pGpio->pPort, pGpio->Channel) & pGpio->Mask) != 0) ? true : false;
}

/**
 * @brief Инвертирование состояния пина
 * @param[in] pGpio Структура с параметрами драйвера GPIO
 */
void GpioToggle(const Gpio_t* pGpio)
{
  assert(pGpio);

  GpioSetValue(pGpio, !GpioGetValue(pGpio));
}

/**
 * @brief Инициализация интерфейса
 * @param[in] GpioPs указатель на структуру в PS ПЛИС
 * @param[in] Channel Канал интерфейса (1 или 2)
 * @param[in] DirectionMask маска направления всех пинов интерфейса.
 * - 0 запись
 * - 1 чтение
 */
void MX_GpioInit(XGpio* GpioPs, unsigned Channel, uint32_t DirectionMask)
{
  s32 Status;

  Status = XGpio_Initialize(GpioPs, XPAR_GPIO_0_DEVICE_ID);

  if(Status == XST_SUCCESS)
    XGpio_SetDataDirection(GpioPs, Channel, DirectionMask );
}
/// @}

#endif /* DT_Z7030 */
