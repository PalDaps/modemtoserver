#if defined(DT_Z7030)

/**
 * @file
 * @brief Заголовочный файл с описанием API GPIO для PicoZed
 */
#ifndef GPIO_DRIVER_PICOZ_H_
#define GPIO_DRIVER_PICOZ_H_

/**
 * @defgroup GpioDriverPicoZ Драйвер GPIO для ПЛИС Zync 7030 PicoZed
 * @ingroup GpioDriver
 * @{
 */
#include "xparameters.h"
#include "xplatform_info.h"
#include "xgpio.h"

#include "gpio_driver.h"

#define GPIO_CH_1      1    ///< Канал интерфейса 1
#define GPIO_CH_2      2    ///< Канал интерфейса 2

#define GPIO_PIN_0     0    ///< Пин интерфейса 0
#define GPIO_PIN_1     1    ///< Пин интерфейса 1
#define GPIO_PIN_2     2    ///< Пин интерфейса 2
#define GPIO_PIN_3     3    ///< Пин интерфейса 3
#define GPIO_PIN_4     4    ///< Пин интерфейса 4
#define GPIO_PIN_5     5    ///< Пин интерфейса 5
#define GPIO_PIN_6     6    ///< Пин интерфейса 6
#define GPIO_PIN_7     7    ///< Пин интерфейса 7
#define GPIO_PIN_8     8    ///< Пин интерфейса 8
#define GPIO_PIN_9     9    ///< Пин интерфейса 9
#define GPIO_PIN_10   10    ///< Пин интерфейса 10
#define GPIO_PIN_11   11    ///< Пин интерфейса 11
#define GPIO_PIN_12   12    ///< Пин интерфейса 12
#define GPIO_PIN_13   13    ///< Пин интерфейса 13
#define GPIO_PIN_14   14    ///< Пин интерфейса 14
#define GPIO_PIN_15   15    ///< Пин интерфейса 15
#define GPIO_PIN_16   16    ///< Пин интерфейса 16
#define GPIO_PIN_17   17    ///< Пин интерфейса 17
#define GPIO_PIN_18   18    ///< Пин интерфейса 18
#define GPIO_PIN_19   19    ///< Пин интерфейса 19
#define GPIO_PIN_20   20    ///< Пин интерфейса 20
#define GPIO_PIN_21   21    ///< Пин интерфейса 21
#define GPIO_PIN_22   22    ///< Пин интерфейса 22
#define GPIO_PIN_23   23    ///< Пин интерфейса 23
#define GPIO_PIN_24   24    ///< Пин интерфейса 24
#define GPIO_PIN_25   25    ///< Пин интерфейса 25
#define GPIO_PIN_26   26    ///< Пин интерфейса 26
#define GPIO_PIN_27   27    ///< Пин интерфейса 27
#define GPIO_PIN_28   28    ///< Пин интерфейса 28
#define GPIO_PIN_29   29    ///< Пин интерфейса 29
#define GPIO_PIN_30   30    ///< Пин интерфейса 30
#define GPIO_PIN_31   31    ///< Пин интерфейса 31

/// @brief Режим работы пина
typedef enum
{
  GPIO_DIR_WRITE = 0,
  GPIO_DIR_READ
} GPIO_Direction;

/// @brief Обертка над GPIO
typedef struct Gpio_
{
  XGpio* pPort;       ///< Порт в PS ПЛИС
  unsigned Channel;   ///< Канал интерфейса
  uint32_t Mask;      ///< Маска пинов
  uint8_t Pin;        ///< Номер пина
} Gpio_t;

void GpioInit(Gpio_t* pGpio, XGpio* pPort, unsigned Channel, uint8_t Pin);
void GpioDeinit(Gpio_t* pGpio);
void MX_GpioInit(XGpio* GpioPs, unsigned Channel, uint32_t DirectionMask);

/// @}

#endif /* GPIO_DRIVER_PICOZ_H_ */

#endif /* DT_Z7030 */
