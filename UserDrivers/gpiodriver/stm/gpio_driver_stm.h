#if defined(STM32)

/**
 * @file
 * @brief Заголовочный файл с описанием API GPIO для STM32
 */
#ifndef GPIO_DRIVER_STM_H_
#define GPIO_DRIVER_STM_H_

/**
 * @addtogroup GpioDriver
 * @{
 */

/**
 * @defgroup GpioDriverStm STM
 * @brief Реализация драйвера для STM
 * @{
 */

#if defined(STM32F756xx) || defined(STM32F746xx) || defined(STM32F745xx) || defined(STM32F765xx) || \
    defined(STM32F767xx) || defined(STM32F769xx) || defined(STM32F777xx) || defined(STM32F779xx) || \
    defined(STM32F722xx) || defined(STM32F723xx) || defined(STM32F732xx) || defined(STM32F733xx) || \
    defined(STM32F730xx) || defined(STM32F750xx)
#include "stm32f7xx.h"
#elif defined(STM32F405xx) || defined(STM32F415xx) || defined(STM32F407xx) || defined(STM32F417xx) || \
    defined(STM32F427xx) || defined(STM32F437xx) || defined(STM32F429xx) || defined(STM32F439xx) || \
    defined(STM32F401xC) || defined(STM32F401xE) || defined(STM32F410Tx) || defined(STM32F410Cx) || \
    defined(STM32F410Rx) || defined(STM32F411xE) || defined(STM32F446xx) || defined(STM32F469xx) || \
    defined(STM32F479xx) || defined(STM32F412Cx) || defined(STM32F412Rx) || defined(STM32F412Vx) || \
    defined(STM32F412Zx) || defined(STM32F413xx) || defined(STM32F423xx)
#include "stm32f4xx.h"
#endif
#include "gpio_driver.h"

/// @brief Обертка над GPIO
typedef struct Gpio_
{
  GPIO_TypeDef* pPort;  ///< Порт
  uint16_t      Pin;    ///< Пин
} Gpio_t;

void GpioInit(Gpio_t* pGpio, GPIO_TypeDef* pPort, uint16_t Pin);
void GpioDeinit(Gpio_t* pGpio);

/// @}

/// @}

#endif /* GPIO_DRIVER_STM_H_ */

#endif /* STM32 */
