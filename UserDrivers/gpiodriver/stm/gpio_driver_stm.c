#if defined(STM32)

/// @addtogroup GpioDriverStm
/// @{

#include "assert.h"
#include "stdbool.h"
#include "gpio_driver_stm.h"

/**
 * @brief Инициализация струкутры
 * @details Gpio_t хранит указатель на порт GPIO. Поэтому порт GPIO должен быть сконструирован и деконструирован извне
 * @param[out] pGpio GPIO
 * @param[in]  pPort Порт
 * @param[in]  Pin   Пин
 */
void GpioInit(Gpio_t* pGpio, GPIO_TypeDef* pPort, uint16_t Pin)
{
  assert(pGpio);

  pGpio->pPort = pPort;
  pGpio->Pin = Pin;
}

/**
 * @brief Деинициализация струкутры
 * @param[in] pGpio GPIO
 */
void GpioDeinit(Gpio_t* pGpio)
{
  assert(pGpio);

  pGpio->pPort = NULL;
  pGpio->Pin = -1;
}

/**
 * @brief Установка значения на пине
 * @param[in] pGpio GPIO
 * @param[in] Value Состояние пина
 */
void GpioSetValue(const Gpio_t* pGpio, bool Value)
{
  assert(pGpio);

  const GPIO_PinState PinState = Value ? GPIO_PIN_SET : GPIO_PIN_RESET;
  HAL_GPIO_WritePin(pGpio->pPort, pGpio->Pin, PinState);
}

/**
 * @brief Чтение значения на пине
 * @param[in] pGpio GPIO
 * @return Состояние пина
 */
bool GpioGetValue(const Gpio_t* pGpio)
{
  assert(pGpio);

  const GPIO_PinState PinState = HAL_GPIO_ReadPin(pGpio->pPort, pGpio->Pin);
  return PinState == GPIO_PIN_SET ? true : false;
}

/**
 * @brief Инвертирование состояния пина
 * @param[in] pGpio GPIO
 */
void GpioToggle(const Gpio_t* pGpio)
{
  assert(pGpio);

  HAL_GPIO_TogglePin(pGpio->pPort, pGpio->Pin);
}

/// @}

#endif
