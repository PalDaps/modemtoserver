/**
 * @file
 * @brief Заголовочный файл с описанием API GPIO
 */
#ifndef GPIO_DRIVER_H_
#define GPIO_DRIVER_H_

/**
 * @defgroup GpioDriver Драйвер GPIO
 * @version 0.1.5
 * @brief Драйвер для GPIO
 * @details Реализация драйвера выбирается define'ом:
 * - STM32
 * - DT_ASIC
 * - DT_Z7030
 * @{
 */

#include "stdbool.h"

/// @cond
struct Gpio_;
typedef struct Gpio_ Gpio_t;
/// @endcond

void GpioSetValue(const Gpio_t* pGpio, bool Value);
bool GpioGetValue(const Gpio_t* pGpio);
void GpioToggle(const Gpio_t* pGpio);

/// @}

#endif /* GPIO_DRIVER_H_ */
