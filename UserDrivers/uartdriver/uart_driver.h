/**
 * @file
 * @brief Заголовочный файл с описанием API UART
 */
#ifndef UART_DRIVER_H_
#define UART_DRIVER_H_

/**
 * @defgroup UartDriver Драйвер UART
 * @version 0.4
 * @brief Драйвер для UART
 * @{
 */

/**
 * @defgroup UartDriverHardware Драйвер для аппаратного порта UART
 * @brief Управление портом UART
 * @{
 */
#include "inttypes.h"
#include "stddef.h"

/// @cond
struct UartPort_;
typedef struct UartPort_ UartPort_t;
/// @endcond

/// @brief Структура интерфейса UART
typedef struct
{
  UartPort_t*  pPort;          ///< Указатель на интерфейс UART
} UartInterface_t;

void UartInterfaceInit(UartInterface_t* pInterface, UartPort_t* pPort);
void UartInterfaceDeinit(UartInterface_t* pInterface);
void UartTransmit(const UartInterface_t* pInterface, void* pData, size_t Size, uint32_t Timeout);
void UartReceive(const UartInterface_t* pInterface, void* pData, size_t Size, uint32_t Timeout);
void UartTransmitDma(const UartInterface_t* pInterface, void* pData, size_t Size);
void UartReceiveDma(const UartInterface_t* pInterface, void* pData, size_t Size);
void UartStopDma(const UartInterface_t* pInterface);
/// @}

/// @}
#endif /* UART_DRIVER_H_ */
