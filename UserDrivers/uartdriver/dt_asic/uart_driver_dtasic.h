#if defined(DT_ASIC)
/**
 * @file
 * @brief Заголовочный файл с описанием API UART для DT ASIC
 */

#ifndef UART_DRIVER_DTASIC_H_
#define UART_DRIVER_DTASIC_H_
/**
 * @addtogroup UartDriver
 * @{
 */
 
 /**
 * @defgroup UartDriverDtAsic
 * @brief Реализация драйвера Uart для DT ASIC
 * @{
 */

#include "assert.h"
#include "stdlib.h"
#include "string.h"
#include "stdio.h"
#include "uart_driver.h"
#include "mpppm.h"
#include "DT_LNS.h"
#include "dt_hal.h"

//UART регистр флагов передатчика//
// Количество свободных байт в FIFO (0 - 256)
#define TX_NT_shift            0
#define TX_NT_mask            0x000001FF
// Количество свободных байт больше заданного порога
#define TX_FR_FL_shift        9
#define TX_FR_FL_mask          0x00000200
// Переполнение передатчика
#define TX_FE_FL_shift        10
#define TX_FE_FL_mask          0x00000400
// Управлния передачей данных (Bit TX_EN_DIS)
#define TX_EN_DIS_FL_shift    11
#define TX_EN_DIS_FL_mask      0x00000800

//UART регистр флагов приемника//
// Количество свободных байт в FIFO (0 - 128)
#define RX_NR_shift            0
#define RX_NR_mask            0x000000FF
// Количество свободных байт больше заданного порога
#define RX_FR_FL_shift        8
#define RX_FR_FL_mask          0x00000100
#define UART_MAX_TX_LEN        256
//UART регистр параметров приемника/передатчика
#define UART_DATA_LNG_shift   0
#define UART_DATA_LNG_mask    0x00000003
#define UART_DATA_STOP_shift  2
#define UART_DATA_STOP_mask   0x00000004
#define UART_DATA_PAR_shift   3
#define UART_DATA_PAR_mask    0x00000038
#define UART_DATA_BRK_shift   6
#define UART_DATA_BRK_mask    0x00000040
//UART регистр конфигурации передатчика// 
#define TX_FR_shift           0
#define TX_FR_mask            0x00000001
#define TX_FE_shift           1
#define TX_FE_mask            0x00000002
#define TX_L_shift            3
#define TX_L_mask             0x00000FF8
#define TX_B_W_shift          12
#define TX_B_W_mask           0x00001000
#define TX_END_shift          13
#define TX_END_mask           0x00002000
//UART регистр конфигурации приемника//
#define RX_FF_shift           0
#define RX_FF_mask            0x00000001
#define RX_FE_shift           1
#define RX_FE_mask            0x00000002
#define RX_L_shift            5
#define RX_L_mask             0x00001FE0
#define RX_B_W_shift          13
#define RX_B_W_mask           0x00002000
#define RX_END_shift          14
#define RX_END_mask           0x00004000
#define PREAMBUL_LEN          5                             // PPM\r\n[5]
#define POSTAMBUL_LEN         14                            // END_LSN_DATA\r\n[14]
#define PPM_WRAPPER_LEN       (PREAMBUL_LEN + POSTAMBUL_LEN)// длина обертки сообщений ППМ



#define  MSG_PPM_HRT_LEN                 8  // PpmHrt\r\n

/// @brief Структура определения длины передаваемых данных UART
typedef enum{
  BITS_5,
  BITS_6,
  BITS_7,
  BITS_8
} WordLen_t;

/// @brief Количество стоповых бит UART
typedef enum{
  STOP_1,
  STOP_2,
} StopBits_t;

/// @brief Способы формирования бита четности
typedef enum{
  NO_PARITY = 0,
  EVEN_PARITY = 1,
  ODD_PARITY = 3,
  CHECK_0 = 5,
  CHECK_1 = 7
} Parity_t;

/// @brief Размер сообщения, передаваемого по UART  
typedef enum{
  BYTE,
  WORD
} DataSize_t;

/// @brief Передача данных с младшего бита или старшего  
typedef enum{
  LITTLE_ENDIAN,
  BIG_ENDIAN
} DataDir_t;

/// @brief Состояние Break Uart
typedef enum{
  BREAK_EN = 0,
  BREAK_DIS
} Break_t;

/// @brief Структура регистров UART в ЦПП ЛСН
typedef struct
{
  volatile uint32_t TxBrr;         /*!< USART Скорость передачи данных,          Address offset: 0x00 */
  volatile uint32_t TxParam;       /*!< USART Параметры передаваемых данных,     Address offset: 0x04 */
  volatile uint32_t TxConfig;      /*!< USART Регистр конфигурации передатчика,  Address offset: 0x08 */
  volatile uint32_t TxData;        /*!< USART Передаваеммые данные,              Address offset: 0x0C */
  volatile uint32_t TxFlag;        /*!< USART Регистр флагов передатчика,        Address offset: 0x10 */
  volatile uint32_t Res1;          /*!< USART Зарезервировано,                   Address offset: 0x14 */
  volatile uint32_t Res2;          /*!< USART Зарезервировано,                   Address offset: 0x18 */
  volatile uint32_t Res3;          /*!< USART Зарезервировано,                   Address offset: 0x1С */
  volatile uint32_t RxBrr;         /*!< USART Скорость приема данных,            Address offset: 0x20 */
  volatile uint32_t RxParam;       /*!< USART Параметры принимаемых данных,      Address offset: 0x24 */
  volatile uint32_t RxConfig;      /*!< USART Регистр конфигурации приемника,    Address offset: 0x28 */
  volatile uint32_t RxData;        /*!< USART Принятые данные,                   Address offset: 0x2С */
  volatile uint32_t RxFlag;        /*!< USART Регистр флагов приемника,          Address offset: 0x30 */
  volatile uint32_t Res4;          /*!< USART Зарезервировано,                   Address offset: 0x34 */
  volatile uint32_t UartConfig;    /*!< USART Конфигурация UART,                 Address offset: 0x38 */
  volatile uint32_t Res5;          /*!< USART Зарезервировано.                   Address offset: 0x3C */
} USART_TypeDef;

/// @brief Структура параметров инициализации UART в ЦПП ЛСН
typedef struct
{
  uint32_t BaudRate;        /*!<  Битовая скорость передачи данных.
                                  BR = (int)((Fc / Fb) + 0.5), где
                                  Fc - частота подаваемая на вход Clk_in, Гц;
                                  Fb - частота передачи данных.
                                  @note BR_min = 5                                                  */
  WordLen_t WordLen;        /*!<  Длинна передаваемых данных (от 5 до 8 бит).                       */
  StopBits_t StopBits;      /*!<  Количество стоповых бит (1 или 2).                                */
  Parity_t Parity;          /*!<  Бит четности посылки.                                             */
  Break_t Break;            /*!<  Управление break.                                                 */ 
  DataSize_t DataSize;      /*!<  Размер сообщения, передаваемого по UART (байт/слово).             */
  DataDir_t DataDir;        /*!<  Передача данных начиная с (младшего/старшего) бита.               */
} UART_InitTypeDef;

/// @brief Обертка над UART
typedef struct UartPort_
{
  USART_TypeDef                     *pPort;       /*!< UART указатель на базовый адресс             */
  UART_InitTypeDef                  TxInit;       /*!< UART communication parameters                */
  UART_InitTypeDef                  RxInit;       /*!< UART communication parameters                */
  char                              *pTxBuffPtr;  /*!< Pointer to UART Tx transfer Buffer           */
  uint32_t                          TxXferSize;   /*!< UART Tx Transfer size                        */
  uint32_t                          TxXferCount;  /*!< UART Tx Transfer Counter                     */
  char                              *pRxBuffPtr;  /*!< Pointer to UART Rx transfer Buffer           */
  uint16_t                          RxXferSize;   /*!< UART Rx Transfer size                        */
  uint16_t                          RxXferCount;  /*!< UART Rx Transfer Counter                     */
//  USL_UART_StateTypeDef             State;        /*!< UART Rx Transfer Counter                     */
} UartPort_t;

void UartInit(UartPort_t* pUart);
void UartDenit(UartPort_t* pUart);
void UartMspInit(UartPort_t* pUart);
void UartMspDeInit(UartPort_t* pUart);
void UartWriteReg(UartPort_t* pUart);

/// @}

/// @}
#endif /* UART_DRIVER_DTASIC_H_ */

#endif /* DT_ASIC */

