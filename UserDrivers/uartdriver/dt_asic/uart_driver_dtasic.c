#if defined(DT_ASIC)

/// @addtogroup UartDriverDtAsic
/// @{

#include "uart_driver_dtasic.h"

static void UartSend(const UartPort_t* pUart, uint32_t Data);
static UslAnswer_t UartTransmitLowLevel(const UartPort_t* pUart, void* pData, size_t Size, uint32_t Timeout);

/**
 * @brief Отправка данных с проверкой таймаута с возвратом статуса операции
 * @details Если размер буфера данных больше чем глубина FIFO отправки,
 * данные записываются в UART по частям.
 * @param[in] pUart Указатель на дескриптор интерфейса
 * @param[in] pData Указатель на буфер данных
 * @param[in] Size Размер буфера данных
 * @param[in] Timeout Время на передачу в мс.
 * @return none
 */
static UslAnswer_t UartTransmitLowLevel(const UartPort_t* pUart, void* pData, size_t Size, uint32_t Timeout)
{
  uint32_t TmpStartTime = 0;  // Начало работы с UART
  uint32_t  TmpLen = 0;       // Размер текущего куска (256 или меньше если хвост)
  uint8_t StepNum = 0;        // Число полностью отправленных 256-байтных частей сообщения
  
  uint16_t BytePerWord = 1;
  if ((pUart->pPort->TxConfig & TX_B_W_mask) == TX_B_W_mask)
    BytePerWord = 4;

  
  TmpStartTime = GetSysTime();

  if ( (pUart->pPort->TxFlag & TX_EN_DIS_FL_mask) == TX_EN_DIS_FL_mask )
  {
    return USL_Fault;
  }
  else
  {
    while (!SysTickTimeout(TmpStartTime, Timeout))
    {
      uint32_t TmpShift = StepNum * UART_MAX_TX_LEN;  // Количество отправленных данных
      uint32_t TmpTail = Size - TmpShift;             // Остаток на отправку
      TmpLen = (TmpTail > UART_MAX_TX_LEN) ? 256 : TmpTail; // Записываем буфер целиком(256), если не хвост
      
      if ( (UART_MAX_TX_LEN - ((pUart->pPort->TxFlag & TX_NT_mask) >> TX_NT_shift)) >= TmpLen) // Если в буфере хватает места для нового куска сообщения
      {
        for(uint32_t TmpCharNumber = 0; TmpCharNumber < (TmpLen / BytePerWord); TmpCharNumber++)
        {
          if ((pUart->pPort->TxConfig & TX_B_W_mask) == BYTE)
            UartSend(pUart, *((char*)pData + TmpCharNumber + TmpShift));
          else
            UartSend(pUart, *((uint32_t*)pData + TmpCharNumber + TmpShift / BytePerWord));
        }

        if (TmpTail == TmpLen)    return USL_OK;
        else                      StepNum++;
      
      }
    }
    return USL_Timeout;
  }
}
/**
 * @brief Отправка одного дескрета данных по Uart
 * @param[in] pUart Указатель на дескриптор интерфейса
 * @param[in] Data дескрет данных
 * @return none
 */
static void UartSend(const UartPort_t* pUart, uint32_t Data)
{
  ((pUart->pPort->TxConfig & TX_B_W_mask) == BYTE) ? (pUart->pPort->TxData = (Data & 0xFF)) : (pUart->pPort->TxData = Data);
  //pUart->pPort->TxData = (Data & 0xFF);
}

/**
 * @brief Отправка данных с проверкой таймаута
 * @details Если размер буфера данных больше чем глубина FIFO отправки,
 * данные записываются в UART по частям.
 * @param[in] pUart Указатель на дескриптор интерфейса
 * @param[in] pData Указатель на буфер данных
 * @param[in] Size Размер буфера данных
 * @param[in] Timeout Время на передачу в мс.
 * @return none
 */
void UartTransmit(const UartInterface_t* pInterface, void* pData, size_t Size, uint32_t Timeout)
{
  UartTransmitLowLevel(pInterface->pPort, pData, Size, Timeout);
}

/**
 * @brief Приём данных с проверкой таймаута
 * @details
 * @param[in] pUart Указатель на дескриптор интерфейса
 * @param[out] pData Указатель на буфер данных
 * @param[in] Size Размер буфера данных
 * @param[in] Timeout Время на приём в мс.
 * @return none
 */
void UartReceive(const UartInterface_t* pInterface, void* pData, size_t Size, uint32_t Timeout)
{
  assert(pInterface);
  assert(pData);
  
  const UartPort_t* pUart = pInterface->pPort;
  
  uint32_t ReceivedCount = 0, TmpStartTime = GetSysTime();
  uint32_t Word = 0x0;
  uint16_t RxDataSize = 0;
  DataSize_t DataSize;
  
  DataSize = (DataSize_t)((pUart->pPort->RxConfig & RX_B_W_mask) == RX_B_W_mask);
  RxDataSize = (pUart->pPort->RxFlag & RX_NR_mask);

    if (RxDataSize != 0)
    {
      while (RxDataSize != 0)
      {       
        if (DataSize == BYTE)
        {
          if (ReceivedCount + 1 > Size)
            return;
          
          Word = (uint8_t)pUart->pPort->RxData;
          *((uint8_t*)pData + ReceivedCount) = Word;
          ReceivedCount++;
          RxDataSize--;          
        }
        else
        {
          Word = pUart->pPort->RxData;
          if ((ReceivedCount + 4 >= Size) || (RxDataSize < 4))
            return;
          for (uint8_t i = 0; i < 4; i++)
          {
            if ((pUart->pPort->RxConfig & RX_END_mask) == RX_END_mask)
              *((uint8_t*)pData + ReceivedCount) = (uint8_t)((Word >> ((3 - i) * 8)) & 0xFF);           
            else
              *((uint8_t*)pData + ReceivedCount) = (uint8_t)((Word >> (i * 8)) & 0xFF);
            ReceivedCount++; 
          }
          RxDataSize -= 4;          
        }
        
        if (SysTickTimeout(TmpStartTime, Timeout))
          return;
      }     
    }
    else
     return;
}

/**
 * @brief Конфигурация интерфейса
 * @param[in] pUart Указатель на дескриптор интерфейса
 * @return none
 */
void UartWriteReg(UartPort_t* pUart)
{
  uint32_t TmpReg = 0x00U;
  
  // Расчет и запись скорости Uart на прием и передачу 
  TmpReg = (uint32_t)(((double)GetClkIn() / (double)pUart->RxInit.BaudRate) + 0.5);
  
  if (TmpReg < 5) 
    TmpReg = 5;
  // (регистры: Скорость передачи данных и Скорость приема данных)
  pUart->pPort->RxBrr = TmpReg;
  
  TmpReg = 0x00U;  
  TmpReg = (uint32_t)(((double)GetClkIn() / (double)pUart->TxInit.BaudRate) + 0.5);
  
  if (TmpReg < 5) 
    TmpReg = 5;
  
  pUart->pPort->TxBrr = TmpReg;
  
  TmpReg = 0x00U;
  // Параметры принимаемых данных 
  TmpReg =  ((((uint32_t)(pUart->RxInit.WordLen)) << UART_DATA_LNG_shift) & UART_DATA_LNG_mask)
          | ((((uint32_t)(pUart->RxInit.StopBits)) << UART_DATA_STOP_shift) & UART_DATA_STOP_mask)
          | ((((uint32_t)(pUart->RxInit.Parity)) << UART_DATA_PAR_shift) & UART_DATA_PAR_mask);
          
  // (регистр: Параметры принимаемых данных)
  pUart->pPort->RxParam = TmpReg;
  
  TmpReg |= ((((uint32_t)(pUart->TxInit.Break)) << UART_DATA_BRK_shift) & UART_DATA_BRK_mask);
  // (регистр: Параметры передаваемых данных)
  pUart->pPort->TxParam = TmpReg;
  
  TmpReg = 0x00U;
  // Конфигурация передатчика
  TmpReg =  ((((uint32_t)(256)) << TX_L_shift) & TX_L_mask) | (pUart->TxInit.DataSize << TX_B_W_shift)
          | (pUart->TxInit.DataDir << TX_END_shift);
  // (регистр: Регистр конфигурации передатчика)
  pUart->pPort->TxConfig = TmpReg;
  
  TmpReg = 0x00U;
  // Конфигурация приемника
  TmpReg =  ((uint32_t)(RX_FF_mask)) | ((((uint32_t)(0x01)) << RX_L_shift) & RX_L_mask)
            | (pUart->RxInit.DataSize << RX_B_W_shift) | (pUart->RxInit.DataDir << RX_END_shift);
  // (регистр: Регистр конфигурации приемника)
  pUart->pPort->RxConfig = TmpReg;
  
  TmpReg = 0x00U;
  // Управление полярностью прерываний IRQ_UART
  TmpReg =  0x02;
  // (регистр: Регистр конфигурации UART)
  pUart->pPort->UartConfig = TmpReg;
  
  TmpReg = 0x00U;
}

/**
 * @brief Отправка по DMA
 * @param[in] pInterface    Указатель на интерфейс UART
 * @param[in] pData         Указатель на данные для отправки
 * @param[in] Size          Размер данных для отправки
 */
void UartTransmitDma(const UartInterface_t* pInterface, void* pData, size_t Size)
{
  return;
}

/**
 * @brief Прием по DMA
 * @param[in] pInterface    Указатель на интерфейс UART
 * @param[in] pData         Указатель на массив для приема
 * @param[in] Size          Размер массива данных для приема
 */
void UartReceiveDma(const UartInterface_t* pInterface, void* pData, size_t Size)
{
  return;
}

/**
 * @brief Остановить прием по DMA
 * @param[in] pInterface    Указатель на интерфейс UART
 */
void UartStopDma(const UartInterface_t* pInterface)
{
  return;
}
/**
  * @brief  Инициализация режима работы Uart
  * @details Для выполнения дополнительных настроек (настройка тактовых сигналов и пр.)
  * функция должна быть переопределена в файле пользователя.
  * @param pUart Указатель на дескриптор интерфейса Uart
  * @param pCg Указатель на дескриптор Clock Generator
  * @return None
  */
void UartInit(UartPort_t* pUart)
{
  assert(pUart);
  UartMspDeInit(pUart);
  UartWriteReg(pUart);
  UartMspInit(pUart);  
}

/**
  * @brief  Деинициализация режима работы Uart
  * @details Для выполнения дополнительных настроек (настройка тактовых сигналов и пр.)
  * функция должна быть переопределена в файле пользователя.
  * @param pUart Указатель на дескриптор интерфейса Uart
  * @return None
  */
void UartDenit(UartPort_t* pUart)
{
  assert(pUart);
  UartMspDeInit(pUart);
}

/**
  * @brief  Инициализация интерфейса Uart
  * @details Для выполнения дополнительных настроек (настройка тактовых сигналов и пр.)
  * функция должна быть переопределена в файле пользователя.
  * @param pUart Указатель на дескриптор интерфейса Uart
  * @return None
  */
__weak void UartMspInit(UartPort_t* pUart)
{
  /* Prevent unused argument(s) compilation warning */
  UNUSED(pUart);
  /* NOTE: This function should not be modified, when the callback is needed,
           the UartMspInit could be implemented in the user file
   */  
}

/**
  * @brief  Деициализация интерфейса Uart
  * @details Для выполнения дополнительных настроек (настройка тактовых сигналов и пр.)
  * функция должна быть переопределена в файле пользователя.
  * @param pUart Указатель на дескриптор интерфейса Uart
  * @return None
  */
__weak void UartMspDeInit(UartPort_t* pUart)
{
  /* Prevent unused argument(s) compilation warning */
  UNUSED(pUart);
  /* NOTE: This function should not be modified, when the callback is needed,
           the UartMspDeInit could be implemented in the user file
   */
}


/// @}

#endif /* defined(DT_ASIC) */
