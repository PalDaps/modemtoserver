#if defined(DT_Z7030)

/// @addtogroup UartDriverPicoZ
/// @{

#include "uart_driver_picoz.h"

/**
 * @brief Инициализация порта UART
 * @param[out] pUart        Указатель на обертку над UART Handle
 * @param[in]  pHandle      Указатель на структуру интерфейса UART
 */
void UartPortInit(UartPort_t* pUart, XUartPs* pHandle)
{
  assert(pUart);
  assert(pHandle);
  pUart->pHandle = pHandle;
#ifdef XUART_IRQ
  pUart->EventData = 0;
  pUart->Event = 0;
#endif
}
/**
 * @brief Деинициализация порта UART
 * @param[out] pUart         Указатель на обертку над Uart Handle
 */
void UartPortDeinit(UartPort_t* pUart)
{
  assert(pUart);
  pUart->pHandle = NULL;
}

/**
 * @brief Отправка данных с проверкой таймаута
 * @details Если размер буфера данных больше чем глубина FIFO отправки,
 * данные записываются в UART по частям.
 * @param[in] pInterface Указатель на интерфейс
 * @param[in] pData Указатель на буфер данных
 * @param[in] Size Размер буфера данных
 * @param[in] Timeout Время на передачу в мс.
 */
void UartTransmit(const UartInterface_t* pInterface, void* pData, size_t Size, uint32_t Timeout)
{
  assert(pInterface);
  assert(pData);

  const UartPort_t* pUart = pInterface->pPort;
  uint32_t TmpStartTime = GetSysTime();

  while(XUartPs_IsSending(pUart->pHandle))
  {
    if (SysTickTimeout(TmpStartTime, Timeout))
      return;
  }

  uint32_t Cnt = 0;
  while (Cnt < Size) {
    Cnt += XUartPs_Send(pUart->pHandle, (uint8_t*)pData + Cnt, (((u32)Size - Cnt) > TX_FIFO_SIZE) ? TX_FIFO_SIZE : (u32)Size - Cnt);

    if (SysTickTimeout(TmpStartTime, Timeout))
      return;
  }
}

/**
 * @brief Приём данных с проверкой таймаута
 * @details
 * @param[in] pInterface Указатель на интерфейс
 * @param[out] pData Указатель на буфер данных
 * @param[in] Size Размер буфера данных
 * @param[in] Timeout Время на приём в мс.
 */
void UartReceive(const UartInterface_t* pInterface, void* pData, size_t Size, uint32_t Timeout)
{
  assert(pInterface);
  assert(pData);

  const UartPort_t* pUart = pInterface->pPort;
  uint32_t ReceivedCount = 0, TmpStartTime = GetSysTime();

  while (ReceivedCount < Size) {
    ReceivedCount += XUartPs_Recv(pUart->pHandle, (uint8_t*)pData + ReceivedCount, (Size - ReceivedCount));

    if (SysTickTimeout(TmpStartTime, Timeout))
      return;
  }
}

/**
 * @brief Инициализация интерфейса
 * @details Функция выполняет проверку и настройку устройства.
 * Для работы с прерываниями от интерфейса необходимо определить
 * символ XUART_IRQ в файлах пользователя, доступных в драйвере.
 * В этом случае при инициализации интерфейса необходимо передать
 * параметр маски прерываний, которые планируется обрабатывать
 * @param [in] pUart Указатель на экземпляр драйвера UART
 * @param [in] DeviceId Id устройства UART. Обычно это значение
 *    XPAR_<UARTPS_instance>_DEVICE_ID из xparameters.h.
 * @param [in] IntrMask Маска прерываний
 * @return  HAL_OK в случае успеха, иначе HAL_ERROR.
 */
#ifdef XUART_IRQ
HAL_StatusTypeDef HAL_UART_Init(UartPort_t* pUart, uint16_t DeviceId, uint32_t IntrMask)
#else
HAL_StatusTypeDef HAL_UART_Init(UartPort_t* pUart, uint16_t DeviceId)
#endif
{
  int32_t Status;
  XUartPs_Config *Config;

  Config = XUartPs_LookupConfig(DeviceId);
  if (NULL == Config) {
    return HAL_ERROR;
  }

  XUartPs_ResetHw(Config->BaseAddress);

  Status = XUartPs_CfgInitialize(pUart->pHandle, Config, Config->BaseAddress);
  if (Status != XST_SUCCESS) {
    return HAL_ERROR;
  }

  Status = XUartPs_SelfTest(pUart->pHandle);
  if (Status != XST_SUCCESS) {
    return HAL_ERROR;
  }

#ifdef XUART_IRQ
  XUartPs_SetHandler(pUart->pHandle, (XUartPs_Handler)UartIrqHandler, pUart);
  XUartPs_SetInterruptMask(pUart->pHandle, IntrMask);
  XUartPs_SetRecvTimeout(pUart->pHandle, 8);
#endif
  XUartPs_SetOperMode(pUart->pHandle, XUARTPS_OPER_MODE_NORMAL);

  return HAL_OK;
}

/**
 * @brief Деинициализация интерфейса
 * @details Функция выполняет проверку и настройку устройства.
 * @param [in] DeviceId Id устройства UART. Обычно это значение
 *    XPAR_<UARTPS_instance>_DEVICE_ID из xparameters.h.
 * @return  HAL_OK в случае успеха, иначе HAL_ERROR.
 */
HAL_StatusTypeDef HAL_UART_DeInit(uint16_t DeviceId)
{
  XUartPs_Config *Config;

  Config = XUartPs_LookupConfig(DeviceId);
  if (NULL == Config) {
    return HAL_ERROR;
  }

  XUartPs_ResetHw(Config->BaseAddress);

  return HAL_OK;
}

#ifdef XUART_IRQ
/** @brief Обработчик прерываний UART
 * @details Сохраняет в структуре UartPort_t данные обо всех прерываниях.
 * Для изменения обработчика, эта функция должна быть
 * переопределена в файлах пользователя.
 * @param CallBackRef
 * @param Event
 * @param EventData
 */
__weak void UartIrqHandler(void *CallBackRef, uint32_t Event, uint32_t EventData)
{
  UartPort_t* pUart = (UartPort_t*)CallBackRef;
  pUart->EventData = EventData;
  pUart->Event = Event;
}
#endif

/**
 * @brief Отправка по DMA
 * @param[in] pInterface    Указатель на интерфейс UART
 * @param[in] pData         Указатель на данные для отправки
 * @param[in] Size          Размер данных для отправки
 */
void UartTransmitDma(const UartInterface_t* pInterface, void* pData, size_t Size)
{
  return;
}

/**
 * @brief Прием по DMA
 * @param[in] pInterface    Указатель на интерфейс UART
 * @param[in] pData         Указатель на массив для приема
 * @param[in] Size          Размер массива данных для приема
 */
void UartReceiveDma(const UartInterface_t* pInterface, void* pData, size_t Size)
{
  return;
}

/**
 * @brief Остановить прием по DMA
 * @param[in] pInterface    Указатель на интерфейс UART
 */
void UartStopDma(const UartInterface_t* pInterface)
{
  return;
}
/// @}
#endif

