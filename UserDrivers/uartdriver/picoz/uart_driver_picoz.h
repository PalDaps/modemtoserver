#if defined(DT_Z7030)
/**
 * @file  uart_driver_picoz.h
 * @brief Заголовочный файл с описанием API UART для Z7030
 */

#ifndef UART_PICOZ_H_
#define UART_PICOZ_H_

/**
 * @defgroup UartDriverPicoZ Драйвер UART для Z7030
 * @ingroup UartDriver
 * @brief Драйвер UART для Z7030 (PicoZed)
 * @{
 */

#include "dt_hal.h"
#include "uart_driver.h"
#include "xuartps.h"

/// @brief Обертка над дескриптором UART
typedef struct UartPort_
{
  XUartPs* pHandle;     ///< Дескриптор UART PS
#ifdef XUART_IRQ
  uint32_t EventData;   ///< Количество принятых байт на момент прерывания.
  uint32_t Event;       ///< Код прерывания (см. xuartps.h)
#endif
} UartPort_t;

#define UART_DEVICE_ID    XPAR_XUARTPS_0_DEVICE_ID        ///< ID устройства

#define RX_FIFO_SIZE      32    ///< Размер аппаратного FIFO для приёма
#define TX_FIFO_SIZE      32    ///< Размер аппаратного FIFO для передачи

void UartPortInit(UartPort_t* pUart, XUartPs* pHandle);
void UartPortDeinit(UartPort_t* pUart);
#ifdef XUART_IRQ
HAL_StatusTypeDef HAL_UART_Init(UartPort_t* pUart, uint16_t DeviceId, uint32_t IntrMask);
void UartIrqHandler(void *CallBackRef, uint32_t Event, uint32_t EventData);
#else
HAL_StatusTypeDef HAL_UART_Init(UartPort_t* pUart, uint16_t DeviceId);
#endif
HAL_StatusTypeDef HAL_UART_DeInit(uint16_t DeviceId);

/// @}
#endif /* UART_PICOZ_H_ */
#endif /* DT_Z7030 */
