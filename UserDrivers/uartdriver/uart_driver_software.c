#include "assert.h"
#include "uart_driver_software.h"

/**
 * @brief Инициализация интерфейса UART в терминах ОСРВ
 * @param[out] pInterface   Указатель на тип программного интерфейса UART
 * @param[in]  pSoftPort    Указатель на программный порт UART, содержащий UART Handle
 */
void UartInterfaceSoftInit(UartInterfaceSoftware_t* pInterface, UartPortSoftware_t* pSoftUart)
{
  assert(pInterface);
  assert(pSoftUart);
  pInterface->pSoftUart = pSoftUart;
}

/**
 * @brief Деинициализация устройства UART в терминах ОСРВ
 * @param[out] pInterface   Указатель на тип программного интерфейса UART
 */
void UartInterfaceSoftDeinit(UartInterfaceSoftware_t* pInterface)
{
  assert(pInterface);
  pInterface->pSoftUart = NULL;
}



