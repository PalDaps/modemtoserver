/**
 * @file
 * @brief Заголовочный файл с описанием API UART для ОСРВ
 */
#ifndef UART_DRIVER_SOFTWARE_H_
#define UART_DRIVER_SOFTWARE_H_

/**
 * @addtogroup UartDriver
 * @{
 */

/**
 * @defgroup UartDriverSoftware Драйвер для программного порта UART
 * @brief Обертка для порта UART в ОСРВ
 * @{
 */
#include "stdbool.h"
#include "stddef.h"
#include "uart_driver.h"
/// @cond
struct UartPortSoftware_;
typedef struct UartPortSoftware_ UartPortSoftware_t;
/// @endcond

/// @brief Устройство UART в терминах ОСРВ
typedef struct
{
  UartPortSoftware_t* pSoftUart;    ///< Указатель на программный порт UART
} UartInterfaceSoftware_t;

void UartInterfaceSoftInit(UartInterfaceSoftware_t* pInterface, UartPortSoftware_t* pSoftUart);
void UartInterfaceSoftDeinit(UartInterfaceSoftware_t* pInterface);
void UartSoftTransmit(const UartInterfaceSoftware_t* pInterface, void* pData, size_t Size, bool UseDma, bool Block);
void UartSoftReceive(const UartInterfaceSoftware_t* pInterface, void* pData, size_t Size, bool UseDma);
void UartSoftStopReceive(const UartInterfaceSoftware_t* pInterface);
/// @}

/// @}
#endif /* UART_DRIVER_SOFTWARE_H_ */
