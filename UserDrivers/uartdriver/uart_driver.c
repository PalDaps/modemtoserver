#include "assert.h"
#include "uart_driver.h"
/**
 * @brief Инициализация интерфейса UART
 * @param[out] pIntrface      Указатель на тип интерфейса UART
 * @param[in]  pPort          Указатель на порт UART, содержащий UART Handle
 */
void UartInterfaceInit(UartInterface_t* pInterface, UartPort_t* pPort)
{
  assert(pInterface);
  assert(pPort);
  pInterface->pPort = pPort;
}

/**
 * @brief Инициализация интерфейса UART
 * @param[out] pIntrface      Указатель на тип интерфейса UART
 */
void UartInterfaceDeinit(UartInterface_t* pInterface)
{
  assert(pInterface);
  pInterface->pPort = NULL;
}
