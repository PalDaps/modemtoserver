#if defined(STM32)

#include "assert.h"
#include "uart_driver_stm.h"
/**
 * @brief Инициализация порта UART
 * @param[out] pUart        Указатель на обертку над UART Handle
 * @param[in]  pHandle      Указатель на структуру интерфейса UART
 */
void UartPortInit(UartPort_t* pUart, UART_HandleTypeDef* pHandle)
{
  assert(pUart);
  assert(pHandle);
  pUart->pHandle = pHandle;
}
/**
 * @brief Деинициализация порта UART
 * @param[out] pUart         Указатель на обертку над Uart Handle
 */
void UartPortDeinit(UartPort_t* pUart)
{
  assert(pUart);
  pUart->pHandle = NULL;
}
/**
 * @brief Отправка
 * @param[in] pInterface    Указатель на интерфейс UART
 * @param[in] pData         Указатель на данные для отправки
 * @param[in] Size          Размер данных для отправки
 * @param[in] Timeout       Таймаут операции
 */
void UartTransmit(const UartInterface_t* pInterface, void* pData, size_t Size, uint32_t Timeout)
{
  assert(pInterface);
  assert(pData);
  HAL_UART_Transmit(pInterface->pPort->pHandle, pData, Size, Timeout);
}
/**
 * @brief Прием
 * @param[in] pInterface    Указатель на интерфейс UART
 * @param[in] pData         Указатель на массив для приема
 * @param[in] Size          Размер массива данных для приема
 * @param[in] Timeout       Таймаут операции
 */
void UartReceive(const UartInterface_t* pInterface, void* pData, size_t Size, uint32_t Timeout)
{
  assert(pInterface);
  assert(pData);
  HAL_UART_Receive(pInterface->pPort->pHandle, pData, Size, Timeout);
}
/**
 * @brief Отправка по DMA
 * @param[in] pInterface    Указатель на интерфейс UART
 * @param[in] pData         Указатель на данные для отправки
 * @param[in] Size          Размер данных для отправки
 */
void UartTransmitDma(const UartInterface_t* pInterface, void* pData, size_t Size)
{
  assert(pInterface);
  assert(pData);
  HAL_UART_Transmit_DMA(pInterface->pPort->pHandle, pData, Size);
}
/**
 * @brief Прием по DMA
 * @param[in] pInterface    Указатель на интерфейс UART
 * @param[in] pData         Указатель на массив для приема
 * @param[in] Size          Размер массива данных для приема
 */
void UartReceiveDma(const UartInterface_t* pInterface, void* pData, size_t Size)
{
  assert(pInterface);
  assert(pData);
  HAL_UART_Receive_DMA(pInterface->pPort->pHandle, pData, Size);
}
/**
 * @brief Остановить прием по DMA
 * @param[in] pInterface    Указатель на интерфейс UART
 */
void UartStopDma(const UartInterface_t* pInterface)
{
  assert(pInterface);
  HAL_UART_DMAStop(pInterface->pPort->pHandle);
}

#endif /* defined(STM32) */
