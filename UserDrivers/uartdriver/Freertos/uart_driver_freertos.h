#if defined(FREERTOS)

/**
 * @file
 * @brief Заголовочный файл с описанием API UART для ОСРВ FreeRTOS
 */
#ifndef UART_DRIVER_FREERTOS_H_
#define UART_DRIVER_FREERTOS_H_
/**
 * @addtogroup UartDriver
 * @{
 */

/**
 * @defgroup UartDriverFreertos FreeRTOS
 * @brief Реализация драйвера для FreeRTOS
 * @{
 */
#include "cmsis_os.h"
#include "queue.h"
#include "uart_driver.h"
#include "uart_driver_software.h"
/// @brief Абстракция над портом UART
typedef struct UartPortSoftware_
{
  UartPort_t*   pUart;  ///< Порт UART
  QueueHandle_t Queue;  ///< Очередь для порта
} UartPortSoftware_t;

void UartPortSoftInit(UartPortSoftware_t* pSoftUart, UartPort_t* pUart, uint8_t NumberItems);
void UartPortSoftDeinit(UartPortSoftware_t* pSoftUart);
void UartSoftLoop(const UartPortSoftware_t* pSoftUart);
/// @}

/// @}
#endif /* UART_DRIVER_FREERTOS_H_ */

#endif /* defined(FREERTOS) */
