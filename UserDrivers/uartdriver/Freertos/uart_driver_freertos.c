#if defined(FREERTOS)

#include "assert.h"
#include "cmsis_os.h"
#include "queue.h"
#include "semphr.h"
#include "util_heap.h"
#include "uart_driver_freertos.h"

/// @cond
typedef enum
{
  TRANSMIT,
  RECEIVE,
  TRANSMIT_DMA,
  RECEIVE_DMA,
  STOP_RECEIVE_DMA
} Mode_t;

typedef struct
{
  void*                           pTxData;
  void*                           pRxData;
  SemaphoreHandle_t               Semphr;
  uint16_t                        TxSize;
  uint16_t                        RxSize;
  Mode_t                          Mode;
} Container_t;
/// @endcond

/**
 * @brief Инициализация программного порта UART
 * @param[out] pSoftPort    Программный порт UART
 * @param[in]  pUart        Порт UART
 * @param[in]  NumberItems  Количество максимальных элементов в очереди
 */
void UartPortSoftInit(UartPortSoftware_t* pSoftUart, UartPort_t* pUart, uint8_t NumberItems)
{
  assert(pSoftUart);
  assert(pUart);
  pSoftUart->pUart = pUart;
  pSoftUart->Queue = xQueueCreate(NumberItems, sizeof(Container_t*));
  assert(pSoftUart->Queue);
}
/**
 * @brief Деинициализация программного порта UART
 * @param[out] pSoftPort Программный порт UART
 */
void UartPortSoftDeinit(UartPortSoftware_t* pSoftUart)
{
  assert(pSoftUart);
  pSoftUart->pUart = NULL;
  vQueueDelete(pSoftUart->Queue);
  pSoftUart->Queue = NULL;
}
/**
 * @brief Отправка по программному порту UART
 * @param[in] pInterface  Указатель на программный интрефейс UART
 * @param[in] pData       Указатель на данные для отправки
 * @param[in] Size        Размер данных для отправки в байтах
 * @param[in] UseDma      Использование DMA при передаче данных
 * @param[in] Block       Ожидание окончания передачи (true - нужно ждать)
 */
void UartSoftTransmit(const UartInterfaceSoftware_t* pInterface, void* pData, size_t Size, bool UseDma, bool Block)
{
  SemaphoreHandle_t Semphr = NULL;

  if (Block)
  {
    Semphr = xSemaphoreCreateBinary();
    assert(Semphr);
  }

  Container_t* pContainer = fpUtilMalloc(sizeof(Container_t));
  pContainer->pTxData = pData;
  pContainer->pRxData = NULL;
  pContainer->Semphr = Semphr;
  pContainer->TxSize = Size;
  pContainer->RxSize = 0;
  pContainer->Mode = UseDma ? TRANSMIT_DMA : TRANSMIT;
  xQueueSendToBack(pInterface->pSoftUart->Queue, &pContainer, portMAX_DELAY);

  if (Block)
  {
    xSemaphoreTake(pContainer->Semphr, portMAX_DELAY);
    vSemaphoreDelete(pContainer->Semphr);
  }

  if (pContainer->Mode == TRANSMIT)
    fpUtilFree(pContainer->pTxData);

  fpUtilFree(pContainer);
}
/**
 * @brief Прием по программному порту UART
 * @param[in] pInterface  Указатель на программный интрефейс UART
 * @param[in] pData       указатель на массив данных для приема
 * @param[in] Size        Размер према в байтах
 * @param[in] UseDma      Использование DMA при приеме данных
 */
void UartSoftReceive(const UartInterfaceSoftware_t* pInterface, void* pData, size_t Size, bool UseDma)
{
  Container_t* pContainer = fpUtilMalloc(sizeof(Container_t));
  pContainer->pTxData = NULL;
  pContainer->pRxData = pData;
  pContainer->Semphr = NULL;
  pContainer->TxSize = 0;
  pContainer->RxSize = Size;
  pContainer->Mode = UseDma ? RECEIVE_DMA : RECEIVE;
  xQueueSendToBack(pInterface->pSoftUart->Queue, &pContainer, portMAX_DELAY);
}
/**
 * @brief Остановить прием через DMA по программному порту
 * @param[in] pInterface  Указатель на программный интрефейс UART
 */
void UartSoftStopReceive(const UartInterfaceSoftware_t* pInterface)
{
  SemaphoreHandle_t Semphr = xSemaphoreCreateBinary();
  assert(Semphr);
  Container_t* pContainer = fpUtilMalloc(sizeof(Container_t));
  pContainer->pTxData = NULL;
  pContainer->pRxData = NULL;
  pContainer->Semphr = Semphr;
  pContainer->TxSize = 0;
  pContainer->RxSize = 0;
  pContainer->Mode = STOP_RECEIVE_DMA;
  xQueueSendToBack(pInterface->pSoftUart->Queue, &pContainer, portMAX_DELAY);
  xSemaphoreTake(Semphr, portMAX_DELAY);
  vSemaphoreDelete(Semphr);
}
/**
 * @brief Обслуживание очереди для программного порта UART
 * @param[in] pSoftPort   Указатель на порт UART, содержащий UART Handle
 */
void UartSoftLoop(const UartPortSoftware_t* pSoftUart)
{
  Container_t* pContainer;

  while (xQueueReceive(pSoftUart->Queue, &pContainer, portMAX_DELAY) == pdTRUE)
  {
    assert(pContainer);
    UartInterface_t UartInterface;
    UartInterfaceInit(&UartInterface, pSoftUart->pUart);

    switch (pContainer->Mode)
    {
      case TRANSMIT:
        UartTransmit(&UartInterface, pContainer->pTxData, pContainer->TxSize, 500);
        break;
      case RECEIVE:
        UartReceive(&UartInterface, pContainer->pRxData, pContainer->RxSize, 500);
        break;
      case TRANSMIT_DMA:
        UartTransmitDma(&UartInterface, pContainer->pTxData, pContainer->TxSize);
        break;
      case RECEIVE_DMA:
        UartReceiveDma(&UartInterface, pContainer->pRxData, pContainer->RxSize);
        break;
      case STOP_RECEIVE_DMA:
        UartStopDma(&UartInterface);
        break;
    }

    if (pContainer->Semphr)
      xSemaphoreGive(pContainer->Semphr);
  }
}

#endif /* defined(FREERTOS) */


