/**
 * @file
 * @brief Заголовочный файл с описанием API строки
 */
#ifndef UTIL_STRING_H_
#define UTIL_STRING_H_

/// @addtogroup Utilities
/// @{

/**
 * @defgroup String Строка
 * @brief API для автоматического управления ресурсами строки
 * @{
 */

#include "stdbool.h"
#include "stdint.h"
#include "util_ring_buffer.h"

/**
 * @brief Структура строки
 * @details Предполагается, что прямое обращение к полям не производится. Ими управляют API функции.
 */
typedef struct
{
  char*     pData;    ///< Указатель на C-строку, место под которую выделено в куче
  uint16_t  Length;   ///< Длина строки
  uint16_t  Capacity; ///< Емкость выделенной строки (максимум возможных символов в выделенной строке)
} String_t;

String_t StrMake();
String_t StrMakeFormCStr(const char* pCStr);
String_t StrMakeFromArray(const char* pArray, uint16_t Size);
String_t StrMakeWithCapacity(uint16_t Size);
String_t StrMakeFromCircularBuffer(const char* pBuffer, size_t BufferSize,
                                   const char* pData, uint16_t DataSize);
String_t StrMakeFromRingBuffer(const RingBuffer_t* pRingBuf, const char* pBegin, const char* pEnd);

/**
 * @brief Получить указатель на C-строку из строки
 * @param[in] pString Указатель на строку
 * @return Указатель на C-строку, используемую в строке
 */
static inline char* StrData(String_t* pString)
{
  return pString->pData;
}

/**
 * @brief Получить указатель на константную C-строку
 * @param[in] pString Указатель на строку
 * @return Указатель на константную C-строку, используемую в строке
 */
static inline const char* StrConstData(const String_t* pString)
{
  return pString->pData;
}

/**
 * @brief Проверка на пустоту строки
 * @details True, если в строке отсутствуют символы.
 * @param[in] pString Указатель на строку
 * @return Логическое значение пустоты строки
 */
static inline bool StrEmpty(const String_t* pString)
{
  return pString->Length == 0;
}

/**
 * @brief Длина строки
 * @param[in] pString Указатель на строку
 * @return Длину строки
 */
static inline uint16_t StrLength(const String_t* pString)
{
  return pString->Length;
}

/**
 * @brief Емкость строки
 * @param[in] pString Указатель на строку
 * @return Емкость строки
 */
static inline uint16_t StrCapacity(const String_t* pString)
{
  return pString->Capacity;
}

String_t StrCopy(const String_t* pString);
void StrResize(String_t* pString, uint16_t Capacity);
void StrClear(String_t* pString);

char* StrAppendChar(String_t* pString, char InsertChar);
char* StrAppendArray(String_t* pString, const char* pInsertArray, uint16_t InsertArraySize);
char* StrAppendCStr(String_t* pString, const char* pInsertCStr);
char* StrAppendString(String_t* pString, const String_t* pInsertString);
char* StrAppendStringOwn(String_t* pString, String_t* pInsertString);

/// @}

/// @}

#endif /* UTIL_STRING_H_ */
