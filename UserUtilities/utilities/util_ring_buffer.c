#include "assert.h"
#include "string.h"
#include "util_ring_buffer.h"

/**
 * @brief Инициализация структуры RingBuffer_t
 * @param[out] pRingBuf Цикличный буфер
 * @param[in] pBuffer Буфер, который используется для цикличной записи
 * @param[in] Size Размер буфера в байтах
 */
void RingBufInit(RingBuffer_t* pRingBuf, void* pBuffer, size_t Size)
{
  assert(pRingBuf);
  assert(pBuffer);

  pRingBuf->pBegin = pBuffer;
  pRingBuf->pEnd = (char*)pBuffer + Size;
}

/**
 * @brief Деинициализация структуры RingBuffer_t
 * @param[out] pRingBuf Цикличный буфер
 */
void RingBufDeinit(RingBuffer_t* pRingBuf)
{
  assert(pRingBuf);

  pRingBuf->pBegin = NULL;
  pRingBuf->pEnd = NULL;
}

/**
 * @brief Цикличное передвижение указателя вперед на заданное число позиций
 * @details Новое значение указателя возвращается из функции. Переданный в нее указатель не изменяется.
 * @param[in] pRingBuf Цикличный буфер
 * @param[in] pCursor Указатель, который необходимо передвинуть
 * @param[in] Value Количество байт, на которое необъодимо передвинуть указатель
 * @return Новое значение указателя
 */
void* RingBufMoveCursor(const RingBuffer_t* pRingBuf, void* pCursor, uint32_t Value)
{
  assert(pRingBuf);
  assert((char*)pCursor >= pRingBuf->pBegin && (char*)pCursor < pRingBuf->pEnd);
  assert(Value < (pRingBuf->pEnd - pRingBuf->pBegin));

  const size_t SpaceLeft = pRingBuf->pEnd - (char*)pCursor;

  return SpaceLeft > Value ? (char*)pCursor + Value : pRingBuf->pBegin + (Value - SpaceLeft);
}

/**
 * @brief Цикличное передвижение указателя назад на заданное число позиций
 * @details Новое значение указателя возвращается из функции. Переданный в нее указатель не изменяется.
 * @param[in] pRingBuf Цикличный буфер
 * @param[in] pCursor Указатель, который необходимо передвинуть
 * @param[in] Value Количество байт, на которое необъодимо передвинуть указатель
 * @return Новое значение указателя
 */
void* RingBufMoveBackCursor(const RingBuffer_t* pRingBuf, void* pCursor, uint32_t Value)
{
  assert(pRingBuf);
  assert((char*)pCursor >= pRingBuf->pBegin && (char*)pCursor < pRingBuf->pEnd);
  assert(Value < (pRingBuf->pEnd - pRingBuf->pBegin));

  const size_t SpaceLeft = (char*)pCursor - pRingBuf->pEnd;

  return SpaceLeft >= Value ? (char*)pCursor - Value : pRingBuf->pEnd - (Value - SpaceLeft);
}

/**
 * @brief Поиск шаблона между переданными указателями
 * @param[in] pRingBuf Цикличный буфер
 * @param[in] pBegin Начало поиска
 * @param[in] pEnd Конец поиска (следующий за последним байтом диапазона поиска)
 * @param[in] pPattern Шаблон поиска
 * @param[in] Size Размер шаблона в байтах
 * @return Указатель на начало шаблона в буфере, если он найден, в противном случае NULL
 */
void* RingBufFind(const RingBuffer_t* pRingBuf, void* pBegin, void* pEnd, const void* pPattern, size_t Size)
{
  assert(pRingBuf);
  assert(((char*)pBegin >= pRingBuf->pBegin) && ((char*)pBegin < pRingBuf->pEnd));
  assert(((char*)pEnd >= pRingBuf->pBegin) && ((char*)pEnd <= pRingBuf->pEnd));
  assert(pPattern);

  while ((char*)pBegin != (char*)pEnd)
  {
    int i = 0;

    for (char* pIterData = (char*)pBegin; i < (int)Size; ++i)
    {
      if (*pIterData != ((uint8_t*)pPattern)[i])
        break;

      pIterData = RingBufMoveCursor(pRingBuf, pIterData, 1);
    }

    if (i == (int)Size)
      return pBegin;

    pBegin = RingBufMoveCursor(pRingBuf, pBegin, 1);
  }

  return NULL;
}

/**
 * @brief Байт между переданными указателями
 * @param[in] pRingBuf Цикличный буфер
 * @param[in] pBegin Начало отсчета
 * @param[in] pEnd Конец отсчета
 * @return Размер в байтах между указателями
 */
size_t RingBufSpaceUsed(const RingBuffer_t* pRingBuf, const void* pBegin, const void* pEnd)
{
  assert(pRingBuf);

  return ((char*)pEnd >= (char*)pBegin ? ((char*)pEnd - (char*)pBegin) : ((pRingBuf->pEnd - (char*)pBegin) + ((char*)pEnd - pRingBuf->pBegin)));
}

/**
 * @brief Копирование данных из буфера
 * @param[in] pRingBuf Цикличный буфер
 * @param[in] pBegin Начало копирования
 * @param[in] pEnd Конец копирования (следующий за последним байтом диапазона копирования)
 * @param[out] pData Место, куда будут скопированы данные
 * @return Количество фактически скопированных байт
 */
size_t RingBufCopy(const RingBuffer_t* pRingBuf, const void* pBegin, const void* pEnd, void* pData)
{
  assert(pRingBuf);
  assert((char*)pBegin >= pRingBuf->pBegin && (char*)pBegin < pRingBuf->pEnd);
  assert((char*)pEnd >= pRingBuf->pBegin && (char*)pEnd <= pRingBuf->pEnd);
  assert(pData);

  size_t Size = 0;

  if ((char*)pEnd >= (char*)pBegin)
  {
    Size = (char*)pEnd - (char*)pBegin;
    memcpy(pData, pBegin, Size);
  }
  else
  {
    Size = pRingBuf->pEnd - (char*)pBegin;
    memcpy(pData, pBegin, Size);
    const size_t CopySize2 = (char*)pEnd - pRingBuf->pBegin;
    memcpy(&((char*)pData)[Size], pRingBuf->pBegin, CopySize2);
    Size += CopySize2;
  }

  return Size;
}

/**
 * @brief Копирование данных из буфера
 * @param[in] pRingBuf Цикличный буфер
 * @param[in] pBegin Начало копирования
 * @param[in] Size Размер копирования
 * @param[out] pData Место, куда будут скопированы данные
 * @return Количество фактически скопированных байт
 */
size_t RingBufCopyToSize(const RingBuffer_t* pRingBuf, const void* pBegin, size_t Size, void* pData)
{
  assert(pRingBuf);
  assert((char*)pBegin >= pRingBuf->pBegin && (char*)pBegin < pRingBuf->pEnd);
  assert((int)Size <= (pRingBuf->pEnd - pRingBuf->pBegin));
  assert(pData);

  size_t TailSize = 0;
  char* pEnd = RingBufMoveCursor(pRingBuf, (void*)pBegin, Size);

  if ((char*)pEnd >= (char*)pBegin)
  {
    memcpy(pData, pBegin, Size);
  }
  else
  {
    TailSize = pRingBuf->pEnd - (char*)pBegin;
    memcpy(pData, pBegin, TailSize);
    Size -= TailSize;
    memcpy(&((char*)pData)[TailSize], pRingBuf->pBegin, Size);
  }

  return Size + TailSize;
}
