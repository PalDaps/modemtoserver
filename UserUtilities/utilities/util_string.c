#include "string.h"
#include "util_heap.h"
#include "util_string.h"

#define DEFAULT_CAPACITY  8

/**
 * @brief Создание пустой строки
 * @details Выделение места в куче не производится.
 * @return Сруктуру инициализированной строки
 */
String_t StrMake()
{
  String_t RetVal =
  {
    .pData = NULL,
    .Length = 0,
    .Capacity = 0,
  };

  return RetVal;
}

/**
 * @brief Создание строки из C-строки
 * @details Место в куче выделяется ровно под необходимую строку.
 * @param[in] pCStr Указатель на C-строку
 * @return Сруктуру инициализированной строки
 */
String_t StrMakeFormCStr(const char* pCStr)
{
  const uint16_t Len = strlen(pCStr);
  const bool LessMaxLength = Len <= 0xFFFF;
  String_t RetVal =
  {
    .pData = LessMaxLength ? fpUtilMalloc(Len + 1) : NULL,
    .Length = 0,
    .Capacity = 0,
  };

  if (RetVal.pData)
  {
    strcpy(RetVal.pData, pCStr);
    RetVal.Length = Len;
    RetVal.Capacity = Len;
  }

  return RetVal;
}

/**
 * @brief Создание строки из массива символов
 * @details Место в куче выделяется ровно под необходимую строку.
 *          Содержание массива остается на совести программиста: непечатаемые символы будут скопированы в
 *          строку как есть.
 * @param[in] pArray Указатель на массив символов
 * @param[in] Size Количество символов, которые необходимо скопировать в строку
 * @return Сруктуру инициализированной строки
 */
String_t StrMakeFromArray(const char* pArray, uint16_t Size)
{
  String_t RetVal =
  {
    .pData =fpUtilMalloc(Size + 1),
    .Length = 0,
    .Capacity = 0,
  };

  if (RetVal.pData)
  {
    RetVal.Length = Size;
    RetVal.Capacity = Size;
    memcpy(RetVal.pData, pArray, Size);
  }

  return RetVal;
}

/**
 * @brief Создание пустой строки с заранее выдеденным местом
 * @details Место в куче выделяется согласно аргументу.
 *          Строка остается в валидном состоянии (первым символом выделенной памяти - '\0')
 * @param[in] Size Емкость строки
 * @return Сруктуру инициализированной строки
 */
String_t StrMakeWithCapacity(uint16_t Size)
{
  String_t RetVal =
  {
    .pData = fpUtilMalloc(Size + 1),
    .Length = 0,
    .Capacity = 0,
  };

  if (RetVal.pData)
  {
    RetVal.Capacity = Size;
    RetVal.pData[0] = '\0';
  }

  return RetVal;
}

/**
 * @brief Создание строки из цикличного буфера
 * @details Место в куче выделяется ровно под необходимую строку.
 *          Содержание массива остается на совести программиста: непечатаемые символы будут скопированы в
 *          строку как есть.
 * @param[in] pBuffer Указатель на начало буфера
 * @param[in] BufferSize Размер буфера
 * @param[in] pData Указатель на начало строки, которую необходимо скопировать из буфера
 * @param[in] DataSize Размер копируемой строки
 * @return Сруктуру инициализированной строки
 */
String_t StrMakeFromCircularBuffer(const char* pBuffer, size_t BufferSize,
                                   const char* pData, uint16_t DataSize)
{
  String_t RetVal =
  {
    .pData = fpUtilMalloc(DataSize + 1),
    .Length = 0,
    .Capacity = 0,
  };

  if (RetVal.pData)
  {
    RetVal.Length = DataSize;
    RetVal.Capacity = DataSize;
    const char* pEndData = pData + DataSize;

    if (pEndData > pData)
    {
      memcpy(RetVal.pData, pData, RetVal.Length);
    }
    else
    {
      const uint16_t TailSize = pBuffer + BufferSize - pData;
      const uint16_t HeadSize = pEndData - pBuffer;
      memcpy(&RetVal.pData[0], pData, TailSize);
      memcpy(&RetVal.pData[TailSize], pBuffer, HeadSize);
    }

    RetVal.pData[RetVal.Length] = '\0';
  }

  return RetVal;
}

/**
 * @brief Создание строки из RingBuffer_t
 * @details Место в куче выделяется ровно под необходимую строку.
 *          Содержание массива остается на совести программиста: непечатаемые символы будут скопированы в
 *          строку как есть.
 * @param[in] pRingBuf Цикличный буфер RingBuffer_t
 * @param[in] pBegin Указатель (в пределах буфера) на начало строки, которую необходимо скопировать из буфера
 * @param[in] pEnd Указатель (в пределах буфера) на конец строки, которую необходимо скопировать из буфера
 *                 (указывает на следующий за последним символом строки)
 * @return Сруктуру инициализированной строки
 */
String_t StrMakeFromRingBuffer(const RingBuffer_t* pRingBuf, const char* pBegin, const char* pEnd)
{
  const uint16_t DataSize = RingBufSpaceUsed(pRingBuf, pBegin, pEnd);

  String_t RetVal =
  {
    .pData = fpUtilMalloc(DataSize + 1),
    .Length = 0,
    .Capacity = 0,
  };

  if (RetVal.pData)
  {
    RetVal.Length = DataSize;
    RetVal.Capacity = DataSize;
    RingBufCopy(pRingBuf, pBegin, pEnd, RetVal.pData);
    RetVal.pData[RetVal.Length] = '\0';
  }
  return RetVal;
}

/**
 * @brief Копирование строки из уже существующей строки
 * @param[in] pString Строка, которую необходимо скопировать
 * @return Сруктуру инициализированной строки
 */
String_t StrCopy(const String_t* pString)
{
  String_t RetVal =
  {
    .pData = pString->Length > 0 ? fpUtilMalloc(pString->Length + 1) : NULL,
    .Length = pString->Length,
    .Capacity = pString->Length,
  };

  if (RetVal.pData) {
    RetVal.Length = pString->Length;
    RetVal.Capacity = pString->Length;
    strcpy(RetVal.pData, pString->pData);
  }

  return RetVal;
}

/**
 * @brief Изменение максимального количества символов в строке
 * @details Строка после resize остается в валидном состоянии:
 *          при увеличении размера - будет скопирована в новое место;
 *          при уменьшении размера - будет скопирована в новое место и обрезана.
 * @param[in] pString Указатель на строку
 * @param[in] Capacity Новое значение емкости строки
 */
void StrResize(String_t* pString, uint16_t Capacity)
{
  if (pString->Capacity != Capacity)
  {
    const uint16_t NewLength = Capacity > pString->Length ? pString->Length : Capacity;
    String_t NewString =
    {
      .pData = Capacity > 0 ? fpUtilMalloc(Capacity + 1) : NULL,
      .Length = 0,
      .Capacity = 0,
    };

    if (NewString.pData)
    {
      NewString.Length = NewLength;
      NewString.Capacity = Capacity;
    }

    if (NewLength > 0)
    {
      memcpy(NewString.pData, pString->pData, NewLength);
      NewString.pData[NewString.Length] = '\0';
    }

    fpUtilFree(pString->pData);
    *pString = NewString;
  }
}

/**
 * @brief Удаление строки
 * @details Строка останется в валидном состоянии, даже если она создана статически
 *          (после удаления будет абсолютно пустой)
 * @param[in] pString Указатель на строку
 */
void StrClear(String_t* pString)
{
  fpUtilFree(pString->pData);
  pString->pData = NULL;
  pString->Length = 0;
  pString->Capacity = 0;
}

static void DoublingCapacity(String_t* pString, uint16_t Capacity)
{
  uint16_t IterableCapacity = pString->Capacity > 0 ? pString->Capacity : Capacity;

  while (IterableCapacity < Capacity)
    IterableCapacity *= 2;

  StrResize(pString, IterableCapacity);
}

/**
 * @brief Добавление символа в конец строки
 * @param[in] pString Указатель на строку
 * @param[in] InsertChar Символ, который необходимо добавить
 * @return Указатель на место вставленного символа в строке
 */
char* StrAppendChar(String_t* pString, char InsertChar)
{
  const uint16_t NeedLength = pString->Length + 1;

  if (NeedLength > pString->Capacity)
    DoublingCapacity(pString, NeedLength);

  char* RetVal = &pString->pData[pString->Length];

  pString->pData[pString->Length++] = InsertChar;
  pString->pData[pString->Length] = '\0';

  return RetVal;
}

/**
 * @brief Добавление массива символов в конец строки
 * @param[in] pString Указатель на строку
 * @param[in] pInsertArray Указатель на массив символов
 * @param[in] InsertArraySize Размер массива символов
 * @return Указатель на начало вставленного массива символов в строке
 */
char* StrAppendArray(String_t* pString, const char* pInsertArray, uint16_t InsertArraySize)
{
  const uint16_t NeedLength = pString->Length + InsertArraySize;

  if (NeedLength > pString->Capacity)
    DoublingCapacity(pString, NeedLength);

  char* RetVal = &pString->pData[pString->Length];

  memcpy(&pString->pData[pString->Length], pInsertArray, InsertArraySize);
  pString->Length += InsertArraySize;
  pString->pData[pString->Length] = '\0';

  return RetVal;
}

/**
 * @brief Добавлеине C-строки в конец строки
 * @param[in] pString Указатель на строку
 * @param[in] pInsertCStr Указатель на C-строку
 * @return Указатель на начало вставленной C-строки в строке
 */
char* StrAppendCStr(String_t* pString, const char* pInsertCStr)
{
  const size_t CStrLen = strlen(pInsertCStr);
  char* RetVal = NULL;

  if (CStrLen <= 0xFFFF)
  {
    const uint16_t NeedLength = pString->Length + CStrLen;

    if (NeedLength > pString->Capacity)
      DoublingCapacity(pString, NeedLength);

    RetVal = &pString->pData[pString->Length];

    strcpy(pString->pData, pInsertCStr);
    pString->Length += CStrLen;
  }

  return RetVal;
}

/**
 * @brief Добавление строки в конец строки
 * @param[in] pString Указатель на строку
 * @param[in] pInsertString Указатель на вставляемую строку
 * @return Указатель на начало вставленной строки в строке
 */
char* StrAppendString(String_t* pString, const String_t* pInsertString)
{
  const uint16_t NeedLength = pString->Length + pInsertString->Length;

  if (NeedLength > pString->Capacity)
    DoublingCapacity(pString, NeedLength);

  char* RetVal = &pString->pData[pString->Length];

  strcpy(&pString->pData[pString->Length], pInsertString->pData);
  pString->Length += pInsertString->Length;

  return RetVal;
}

/**
 * @brief Добавление строки в конец строки с передачей владения
 * @details Если сторка пустая, то значения полей просто копируются
 *          без выделения новой памяти и копирования C-строк
 * @param[in] pString Указатель на строку
 * @param[in] pInsertString Указатель на вставляемую строку
 * @return Указатель на начало вставленной строки в строке
 */
char* StrAppendStringOwn(String_t* pString, String_t* pInsertString)
{
  if (pString->pData)
  {
    char* RetVal = StrAppendString(pString, pInsertString);
    StrClear(pInsertString);
    return RetVal;
  }

  *pString = *pInsertString;
  pInsertString->pData = NULL;
  pInsertString->Length = 0;
  pInsertString->Capacity = 0;

  return pString->pData;
}
