/**
 * @file
 * @brief Заголовочный файл с описанием API для выделения памяти на куче
 * @detailes Для работы с библиотекой внутри операционной системы FreeRTOS
 * следует добавить препроцессорную директиву (#define FREERTOS).
 */
#ifndef UTIL_HEAP_H_
#define UTIL_HEAP_H_

/// @addtogroup Utilities
/// @{

/**
 * @defgroup Heap Куча
 * @brief API для выделения памяти в куче
 * @{
 */

#include "stddef.h"

extern void* (*fpUtilMalloc)(size_t);
extern void (*fpUtilFree)(void*);

/// @}

/// @}

#endif /* UTIL_HEAP_H_ */
