/**
 * @file
 * @brief Заголовочный файл с описанием API управления цикличным буфером
 */
#ifndef UTIL_RING_BUFFER_H_
#define UTIL_RING_BUFFER_H_

/// @addtogroup Utilities
/// @{

/**
 * @defgroup RingBuffer Цикличный буфер
 * @brief API управления цикличным буфером
 * @details Включает несколько базовых функций, упрощающих управление цикличным буфером.
 * @{
 */

#include "stddef.h"
#include "stdint.h"

/**
 * @brief Структура цикличного буфера
 * @details Прямое обращение к полям не предполагается.
 */
typedef struct
{
  char* pBegin; ///< Указатель на начало буфера
  char* pEnd;   ///< Указатель на конец буфера
} RingBuffer_t;

void RingBufInit(RingBuffer_t* pRingBuf, void* pBuffer, size_t Size);
void RingBufDeinit(RingBuffer_t* pRingBuf);

void* RingBufFind(const RingBuffer_t* pRingBuf, void* pBegin, void* pEnd, const void* pPattern, size_t Size);
size_t RingBufSpaceUsed(const RingBuffer_t* pRingBuf, const void* pBegin, const void* pEnd);
void* RingBufMoveCursor(const RingBuffer_t* pRingBuf, void* pCursor, uint32_t Value);
void* RingBufMoveBackCursor(const RingBuffer_t* pRingBuf, void* pCursor, uint32_t Value);
size_t RingBufCopy(const RingBuffer_t* pRingBuf, const void* pBegin, const void* pEnd, void* pData);
size_t RingBufCopyToSize(const RingBuffer_t* pRingBuf, const void* pBegin, size_t Size, void* pData);

/// @}

/// @}

#endif /* UTIL_RING_BUFFER_H_ */
