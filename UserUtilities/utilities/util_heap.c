#include "util_heap.h"
#if defined FREERTOS
#include "cmsis_os.h"
#else
#include "stdlib.h"
#endif
/**
 * @brief Указатель на функцию выделения памяти
 * @details Данный указатель используется во всем проекте Utilities
 */
void* (*fpUtilMalloc)(size_t Size) =
#if defined FREERTOS
  pvPortMalloc;
#else
  malloc;
#endif

/**
 * @brief Указатель на функцию освобождение памяти из кучи
 * @details Данный указатель используется во всем проекте Utilities
 */

void (*fpUtilFree)(void* pHeapData) =
#if defined FREERTOS
  vPortFree;
#else
  free;
#endif

