/**
  * @file    pn6280.h
  * @author  Dashko P.P. (NIIMA PROGRESS)
  * @brief   Заголовочный файл с описанием функций работы с модулем ПН6280
  *
  * Файл содержит описание определений, структур и функций API для
  * модуля радионавигационной опорной станции ПН6280.
  */

#ifndef MODEM_H_
#define MODEM_H_

/**
 * @defgroup PN6280
 * @version 0.1
 * @brief Описание структуры состояний, определений и функций API
 * @{
 */

/**
 * @defgroup PN6280_Definitions
 * @brief Определения
 * @{
 */

/**
 * @defgroup PN6280_Defines
 * @brief Макросы
 * @{
 */

#include "pn_protocol.h"
#include "data_flow_control.h"

/**
 * @defgroup PN6280_Define0 Общие определения
 * @{
 */
#define CONFIG_PERIOD_MAX_CNT     9

#define PN6280_UART_BUFF_LEN      10240
/// @}

#define PN6280_PWRON_PORT         GPIOE
#define PN6280_PWRON_PIN			 		GPIO_PIN_10
#define PN6280_MODEM_RESET_PORT   GPIOD
#define PN6280_MODEM_RESET_PIN		GPIO_PIN_0
#define PN6280_WAKE_PORT          GPIOB
#define PN6280_WAKE_PIN				 		GPIO_PIN_6

/// @}

typedef void(*SendToUsb)(const char* pMsg, size_t Size);
typedef void(*SendToUartRos)(const char* pMsg, size_t Size);

/**
 * @defgroup PN6280_Structures
 * @brief Структуры
 * @{
 */
 /// @brief Структура для коллбеков необходимых для модуля PN6280
typedef struct
{
  SendToUsb 											fpSendToUser;
  SendToUartRos 									fpSendToUartRos;
} ModemUserCallbacks_t;

typedef enum
{
  modSockClose,
  modSockOpen,
  modSockWait   // Ожидание ответа от модема
} ModemSockStat;

typedef enum
{
  modNetNone,
  modNetHome    = '1',
  modNetRoaming = '5'
} ModemNetwork;

typedef enum
{
  SIM_UNDEF = 0,
  SIM_OTHER,
  SIM_GLONASS = 77,
} SimChip_t;

typedef enum
{
  MODEM_UNDEF = 0,
  MODEM_PN6280,
	MODEM_TELITUE866,
} ModemType_t;

typedef enum
{
	PN_UNDEF,               ///< Неопределенное состояние ответа
	PN_AT,									///< Активация AT команд
	PN_IMSI,								///<
	PN_CMEE,								///< Активация расширенной ифнормации об ошибках
	PN_CGMI,								///< Запрос фирмы производителя
	PN_CICCID,							///< Считывание ICCID SIM-карты и посмотреть вставлена ли SIM-карта
	PN_CPAS_GET_STATE,			///< Просмотр информации о состоянии модуля
	PN_CPAS_GET_PARAMS,			///<
	PN_ESIMS,								///<
	PN_ESIMS_GET_STATE,			///<
	PN_CNUM,								///<
	PN_ECUSD,								///<
	PN_ESIMINFO,						///<
	PN_ESPN,								///<
	PN_CSQ,									///< Запрос уровня сигнала
	PN_CPIN,								///< Настройка пароля SIM-карты
	PN_CIMI,								///< Запрос IMSI SIM-карты
	PN_CFUN,								///<
	PN_COPS_GET_STATE,			///<
	PN_COPS_SET_PARAMS,			///<
	PN_COPS_GET_PARAMS,			///<
	PN_CGREG_GET_STATE, 		///<
	PN_CGREG_SET_PARAMS,    ///<
	PN_CSTT_GET_STATE,			///<
	PN_CSTT_GET_PARAMS,			///<
	PN_CSTT_SET_PARAMS,			///<
	PN_CIICR,								///<
	PN_CGDATA,							///<
	PN_CIPMUX,							///<
	PN_CMGF,         				///<
	PN_CMGS,                ///<
	PN_SEND_MSG,            ///<
	PN_READY,								///<
	PN_CIPSTART,            ///<
	PN_CGATT,								///<
	PN_CGDCONT_GET_STATE,		///<
	PN_CGPADDR,							///<
	PN_CGPADDR_GET_STATE,		///<
	PN_CGPADDR_GET_PARAMS,	///<
	PN_CGDCOUNT,						///<
	PN_ERROR,								///<
	PN_VOID,								///<
}  ModemAtCommand_t;

typedef struct
{
	uint32_t 					  						LastTime;   // Временная метка последней отправленной команды
	uint32_t 					  						Count;      // Счетчик отправленных команд на текущем этапе
	ModemAtCommand_t 	  						Status;     // Этап конфигурации модема
	SimChip_t 				  						Sim;        // Оператор (пока есть разделение, только на Глонасс/Остальные)
	ModemType_t 			  						Type;       // Тип модема
} ModemConfig_t;

typedef struct
{
	Gpio_t*           							pPwron;
	Gpio_t*				    							pModemReset;
	Gpio_t*				    							pWake;
} ModemPwrSettings_t;

typedef struct
{
	char             								Ip[16];     // IP сервера
	char              							Port[5];    // Номер порта сервера
	ModemNetwork      							Network;    // Сеть, в которой находится (домашняя или роуминг)
	ModemSockStat     							Status;     // Статус сокета
	bool             								Auth;       // Статус авторизации на сервере
	bool             								Rtk;        // Статус авторизации на RTK поток
	bool             								Ppp;        // Статус авторизации на PPP поток
} ModemSock_t;
/// @brief Структура состояния модуля PN6280
typedef struct
{
	void* 													pPnProtocol;
	UartInterfaceSoftware_t* 				pUartSoftInterface;
	ModemUserCallbacks_t* 					pCallbacks;
	ModemAtCommand_t 								ModemAtCommand;
	ModemConfig_t* 									pModemConfig;
	ModemSock_t* 										pModemSock;
	ModemPwrSettings_t* 						pModemPwrSettings;
} Modem_t;
/// @}

/// @}

/// @}

/**
 * @defgroup PN6280_Functions
 * @brief Функции API
 * @{
 */
Modem_t* ModemInit(UART_HandleTypeDef* pUartHandle, ModemUserCallbacks_t* pCallback);
void ModemDeinit(Modem_t* pModem);
void ModemProcessing(Modem_t* pModem, DataFlowControl_t* pDataFlowControl);
void ModemSend(Modem_t* pModem, const String_t* pCmd);
void ModemTcpIpConfig(Modem_t* pModem);
void ParserModemAnswer(Modem_t* pModem, const String_t* pStr);
void UpdateGsmSocket(Modem_t* pModem, CommonConnectPoint_t* pPoint, char* pId);
void GsmWriteMultiSocketNonTransporent(void* pHandle, uint8_t SocketNumber, String_t* pStr, SendState_t* State);
/// @}

/// @}
#endif /* MODEM_H_ */

