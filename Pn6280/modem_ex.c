/**
 * @file    pn_protocol_ex.c
 * @author  Dashko P. P. (NIIMA PROGRESS)
 * @brief   Файл с описанием функций формирования команд для модуля PN6280
 */
/// @addtogroup PnProtocol
/// @{
#include "modem.h"
#include "string.h"
#include "stdio.h"
#include "cmsis_os.h"
#include "util_string.h"
#include "data_flow_control.h"
/**
 * @brief Отправляет AT команду с помощью callback вызова по UART
 * @param[in] pPnProtocol      Указатель на структуру состояния парсера моудля PN6280
 * @param[in] pCmd             Указатель на отправляему команду
 */
void ModemSend(Modem_t* pModem, const String_t* pCmd)
{
	assert(pModem);
	assert(pCmd);
	PnProtocol_t* pPnProtocol = (PnProtocol_t*)pModem->pPnProtocol;
	PnTransmit_t fpModemSend = (PnTransmit_t)pPnProtocol->Callbacks->Common->fpTransmit;
	fpModemSend(pModem, pCmd->pData, pCmd->Length);
}
/**
 * @brief Отправляет верную последовательность AT команд, которые открывают сокет на модуле ПН6280
 * @param[in] pPnProtocol      Указатель на структуру состояния моудля PN6280
 */
void ModemTcpIpConfig(Modem_t* pModem)
{
	static uint8_t CycleCnt = 0;
  if (CycleCnt == CONFIG_PERIOD_MAX_CNT)
  {
    uint32_t tmpTimePassed = xTaskGetTickCount() - pModem->pModemConfig->LastTime;
    CycleCnt = 0;
		switch (pModem->pModemConfig->Status)
		{
			case PN_AT:
			{
				if (pModem->pModemConfig->Count == 0 || tmpTimePassed > 500)
				{
					String_t Str = StrMakeFormCStr("AT\r");
					ModemSend(pModem, &Str);
					StrClear(&Str);
					++pModem->pModemConfig->Count;
					pModem->pModemConfig->LastTime = xTaskGetTickCount();
				}
			}
			break;

			case PN_CGMI:
			{
				if (pModem->pModemConfig->Count == 0 || tmpTimePassed > 500)
				{
					String_t Str = StrMakeFormCStr("AT+CGMI\r");
					ModemSend(pModem, &Str);
					StrClear(&Str);
					++pModem->pModemConfig->Count;
					pModem->pModemConfig->LastTime = xTaskGetTickCount();
				}
			}
			break;

			case PN_IMSI:
			{
				if (pModem->pModemConfig->Count == 0 || tmpTimePassed > 500)
				{
					String_t Str = StrMakeFormCStr("AT+CIMI\r");
					ModemSend(pModem, &Str);
					StrClear(&Str);
					++pModem->pModemConfig->Count;
					pModem->pModemConfig->LastTime = xTaskGetTickCount();
				}
			}
			break;

			case PN_CFUN:
			{
				if (pModem->pModemConfig->Count == 0 || tmpTimePassed > 500)
				{
					String_t Str = StrMakeFormCStr("AT+CFUN=1\r");
					ModemSend(pModem, &Str);
					StrClear(&Str);
					++pModem->pModemConfig->Count;
					pModem->pModemConfig->LastTime = xTaskGetTickCount();
				}
			}

			case PN_CMEE:
			{
				if (pModem->pModemConfig->Count == 0 || tmpTimePassed > 500)
				{
					String_t Str = StrMakeFormCStr("AT+CMEE=2\r");
					ModemSend(pModem, &Str);
					StrClear(&Str);
					++pModem->pModemConfig->Count;
					pModem->pModemConfig->LastTime = xTaskGetTickCount();
				}
			}
			break;

			case PN_CGREG_GET_STATE:
			{
				if (pModem->pModemConfig->Count == 0 || tmpTimePassed > 500)
				{
					String_t Str = StrMakeFormCStr("AT+CGREG?\r");
					ModemSend(pModem, &Str);
					StrClear(&Str);
					++pModem->pModemConfig->Count;
					pModem->pModemConfig->LastTime = xTaskGetTickCount();
				}
			}
			break;

			case PN_CGPADDR:
			{
				if (pModem->pModemConfig->Count == 0 || tmpTimePassed > 500)
				{
					String_t Str = StrMakeFormCStr("AT+CGPADDR=1\r");
					ModemSend(pModem, &Str);
					StrClear(&Str);
					++pModem->pModemConfig->Count;
					pModem->pModemConfig->LastTime = xTaskGetTickCount();
				}
			}
			break;

			case PN_CGDCOUNT:
			{
				if (pModem->pModemConfig->Count == 0 || tmpTimePassed > 500)
				{
					if (pModem->pModemConfig->Sim == SIM_OTHER)
					{
						String_t Str = StrMakeFormCStr("AT+CGDCONT=1,\"IP\",\"internet.beeline.ru\"\r");
						ModemSend(pModem, &Str);
						StrClear(&Str);
					}
					else if (pModem->pModemConfig->Sim == SIM_GLONASS)
					{
						String_t Str = StrMakeFormCStr("AT+CGDCONT=1,\"IP\",\"Internet\"\r");
						ModemSend(pModem, &Str);
						StrClear(&Str);
					}

					++pModem->pModemConfig->Count;
					pModem->pModemConfig->LastTime = xTaskGetTickCount();
				}
			}
			break;

			case PN_CSTT_SET_PARAMS:
			{
				if (pModem->pModemConfig->Count == 0 || tmpTimePassed > 500)
				{
					if (pModem->pModemConfig->Sim == SIM_OTHER)
					{
						String_t Str = StrMakeFormCStr("AT+CSTT=\"internet.beeline.ru\",\"IP\",\"beeline\",\"beeline\"\r");
						ModemSend(pModem, &Str);
						StrClear(&Str);
					}
					else if (pModem->pModemConfig->Sim == SIM_GLONASS)
					{
						String_t Str = StrMakeFormCStr("AT+CSTT=\"Internet\",\"IP\",\"\",\"\"\r");
						ModemSend(pModem, &Str);
						StrClear(&Str);
					}

					++pModem->pModemConfig->Count;
					pModem->pModemConfig->LastTime = xTaskGetTickCount();
				}
			}
			break;

			case PN_CIICR:
			{
				if (pModem->pModemConfig->Count == 0 || tmpTimePassed > 5000)
				{
					String_t Str = StrMakeFormCStr("AT+CIICR\r");
					ModemSend(pModem, &Str);
					StrClear(&Str);
					++pModem->pModemConfig->Count;
					pModem->pModemConfig->LastTime = xTaskGetTickCount();
				}
			}
			break;

			case PN_CGDATA:
			{
				if (pModem->pModemConfig->Count == 0 || tmpTimePassed > 5000)
				{
					String_t Str = StrMakeFormCStr("AT+CGDATA=\"M-MBIM\",1,1\r");
					ModemSend(pModem, &Str);
					StrClear(&Str);
					++pModem->pModemConfig->Count;
					pModem->pModemConfig->LastTime = xTaskGetTickCount();
				}
			}
			break;

			case PN_CIPMUX:
			{
				if (pModem->pModemConfig->Count == 0 || tmpTimePassed > 500)
				{
					String_t Str = StrMakeFormCStr("AT+CIPMUX = 0\r");
					ModemSend(pModem, &Str);
					StrClear(&Str);
					++pModem->pModemConfig->Count;
					pModem->pModemConfig->LastTime = xTaskGetTickCount();
				}
			}
			break;

			case PN_CMGF:
			{
				if (pModem->pModemConfig->Count == 0 || tmpTimePassed > 500)
				{
					String_t Str = StrMakeFormCStr("AT+CMGF=1\r");
					ModemSend(pModem, &Str);
					StrClear(&Str);
					++pModem->pModemConfig->Count;
					pModem->pModemConfig->LastTime = xTaskGetTickCount();
				}
			}
			break;

			case PN_CMGS:
			{
				if (pModem->pModemConfig->Count == 0 || tmpTimePassed > 500)
				{
					String_t Str = StrMakeFormCStr("AT+CMGS=+79882500167\r");
					ModemSend(pModem, &Str);
					StrClear(&Str);
					++pModem->pModemConfig->Count;
					pModem->pModemConfig->LastTime = xTaskGetTickCount();
				}
			}
			break;

			case PN_SEND_MSG:
			{
				if (pModem->pModemConfig->Count == 0 || tmpTimePassed > 500)
				{
					String_t Str = StrMakeFormCStr("PN6280 MESSAGE");
					ModemSend(pModem, &Str);
					StrClear(&Str);
					++pModem->pModemConfig->Count;
					pModem->pModemConfig->LastTime = xTaskGetTickCount();
				}
			}
			break;

			case PN_CGREG_SET_PARAMS:
			{
				if (pModem->pModemConfig->Count == 0 || tmpTimePassed > 500)
				{
					String_t Str = StrMakeFormCStr("AT+CGREG=1\r");
					ModemSend(pModem, &Str);
					StrClear(&Str);
					++pModem->pModemConfig->Count;
					pModem->pModemConfig->LastTime = xTaskGetTickCount();
				}
			}
			break;

			case PN_CGATT:
			{
				if (pModem->pModemConfig->Count == 0 || tmpTimePassed > 500)
				{
					String_t Str = StrMakeFormCStr("AT+CGATT?");
					ModemSend(pModem, &Str);
					StrClear(&Str);
					++pModem->pModemConfig->Count;
					pModem->pModemConfig->LastTime = xTaskGetTickCount();
				}
			}
			break;

			case PN_CGDCONT_GET_STATE:
			{
				if (pModem->pModemConfig->Count == 0 || tmpTimePassed > 500)
				{
					String_t Str = StrMakeFormCStr("AT+CGDCONT?");
					ModemSend(pModem, &Str);
					StrClear(&Str);
					++pModem->pModemConfig->Count;
					pModem->pModemConfig->LastTime = xTaskGetTickCount();
				}
			}
			break;

			case PN_CGPADDR_GET_STATE:
			{
				if (pModem->pModemConfig->Count == 0 || tmpTimePassed > 500)
				{
					String_t Str = StrMakeFormCStr("AT+CGPADDR?");
					ModemSend(pModem, &Str);
					StrClear(&Str);
					++pModem->pModemConfig->Count;
					pModem->pModemConfig->LastTime = xTaskGetTickCount();
				}
			}
			break;

			case PN_CGPADDR_GET_PARAMS:
			{
				if (pModem->pModemConfig->Count == 0 || tmpTimePassed > 500)
				{
					String_t Str = StrMakeFormCStr("AT+CGPADDR=?");
					ModemSend(pModem, &Str);
					StrClear(&Str);
					++pModem->pModemConfig->Count;
					pModem->pModemConfig->LastTime = xTaskGetTickCount();
				}
			}
			break;

			case PN_CSTT_GET_STATE:
			{
				if (pModem->pModemConfig->Count == 0 || tmpTimePassed > 500)
				{
					String_t Str = StrMakeFormCStr("AT+CSTT?");
					ModemSend(pModem, &Str);
					StrClear(&Str);
					++pModem->pModemConfig->Count;
					pModem->pModemConfig->LastTime = xTaskGetTickCount();
				}
			}
			break;
/*
			case PN_CGREG_GET_STATE:
			{
				String_t Str = StrMakeFormCStr("AT+CGREG?");
				ModemSend(pModem, &Str);
				StrClear(&Str);
				++pModem->pModemConfig->Count;
				pModem->pModemConfig->LastTime = xTaskGetTickCount();
			}
			break;
*/

			case PN_READY:
			case PN_ERROR:
			default:
			break;
		}
  }
  else
  	CycleCnt++;

}

void ParserModemAnswer(Modem_t* pModem, const String_t* pStr)
{
  switch (pModem->pModemConfig->Status)
  {
    case PN_UNDEF:
    {
      if (strstr(pStr->pData, "TCPIP OK\r\n"))
        pModem->pModemConfig->Status = PN_AT;
    }
    break;

    case PN_AT:
    {
      if (strstr(pStr->pData, "OK\r\n"))
        pModem->pModemConfig->Status = PN_CGMI;
    }
    break;

    case PN_CGMI:
    {
      if (strstr(pStr->pData, "OK\r\n"))
      {
        if (strstr(pStr->pData, "JSC MRI PROGRESS\r\n"))
          pModem->pModemConfig->Type = MODEM_PN6280;
        else if (strstr(pStr->pData, "Telit\r\n"))
          pModem->pModemConfig->Type = MODEM_TELITUE866;

        pModem->pModemConfig->Status = PN_IMSI;
      }
    }
    break;

    case PN_IMSI:     // Report Mobile Equipment Error
    {
      if (strstr(pStr->pData, "OK\r\n"))
      {
        pModem->pModemConfig->Count = 0;
        pModem->pModemConfig->Status = PN_CFUN;
      }
    }
    break;

    case PN_CFUN:
    {
      if (strstr(pStr->pData, "OK\r\n"))
        pModem->pModemConfig->Status = PN_CMEE;
    }
    break;

    case PN_CMEE:
    {
      if (strstr(pStr->pData, "OK\r\n"))
      {
        pModem->pModemConfig->Count = 0;
        pModem->pModemConfig->Status = PN_CGREG_GET_STATE;
      }
    }
    break;

    case PN_CGREG_GET_STATE:
    {
      if (strstr(pStr->pData, "+CGREG"))
      {
        char tmpState = *(strchr(pStr->pData, ',') + 2);
        if (strstr(pStr->pData, "+CGREG: 1, 1"))
        {
          pModem->pModemConfig->Count = 0;
          pModem->pModemConfig->Status = PN_CMGF;
          break;
        }
        if (tmpState == modNetHome || tmpState == modNetRoaming)
        {
          pModem->pModemSock->Network = tmpState;
        }
      }
      if (strstr(pStr->pData, "OK\r\n") && pModem->pModemSock->Network != modNetNone)
      {
        pModem->pModemConfig->Count = 0;
        pModem->pModemConfig->Status = PN_CGPADDR;
      }
    }
    break;

    case PN_CGPADDR:
    {
      if (strstr(pStr->pData, "OK\r\n"))
      {
        pModem->pModemConfig->Count = 0;
        pModem->pModemConfig->Status = PN_CGDCOUNT;
      }
    }
    break;

    case PN_CGDCOUNT:
    {
      if (strstr(pStr->pData, "OK\r\n"))
      {
        pModem->pModemConfig->Count = 0;
        pModem->pModemConfig->Status = PN_CSTT_SET_PARAMS;
      }
    }
    break;

    case PN_CSTT_SET_PARAMS:
    {
      if (strstr(pStr->pData, "OK\r\n"))
      {
        pModem->pModemConfig->Count = 0;
        pModem->pModemConfig->Status = PN_CIICR;
      }
    }
    break;

    case PN_CIICR:
    {
      if (strstr(pStr->pData, "OK\r\n"))
      {
        pModem->pModemConfig->Count = 0;
        pModem->pModemConfig->Status = PN_CGDATA;
      }
    }
    break;

    case PN_CGDATA:
    {
      if (strstr(pStr->pData, "OK\r\n"))
      {
        pModem->pModemConfig->Count = 0;
        pModem->pModemConfig->Status = PN_CIPMUX;
      }
    }
    break;

    case PN_CIPMUX:
    {
      if (strstr(pStr->pData, "OK\r\n"))
      {
        pModem->pModemConfig->Count = 0;
        pModem->pModemConfig->Status = PN_CGREG_SET_PARAMS;
      }
    }
    break;

    case PN_CGREG_SET_PARAMS:
    {
      if (strstr(pStr->pData, "OK\r\n"))
      {
        pModem->pModemConfig->Count = 0;
        pModem->pModemConfig->Status = PN_CGATT;
        pModem->pModemConfig->Status = PN_VOID;
      }
    }
    break;

    case PN_CGATT:
    {
      if (strstr(pStr->pData, "OK\r\n"))
      {
        pModem->pModemConfig->Count = 0;
        pModem->pModemConfig->Status = PN_CGDCONT_GET_STATE;
      }
    }
    break;

    case PN_CGDCONT_GET_STATE:
		{
      if (strstr(pStr->pData, "OK\r\n") || strstr(pStr->pData, "Internet"))
      {
        pModem->pModemConfig->Count = 0;
        pModem->pModemConfig->Status = PN_CGPADDR_GET_STATE;
      }
		}
    break;

    case PN_CGPADDR_GET_STATE:
    {
      if (strstr(pStr->pData, "OK\r\n") || strstr(pStr->pData, "ERROR"))
      {
        pModem->pModemConfig->Count = 0;
        pModem->pModemConfig->Status = PN_CGPADDR_GET_PARAMS;
      }
    }
    break;

    case PN_CGPADDR_GET_PARAMS:
    {
      if (strstr(pStr->pData, "OK\r\n"))
      {
        pModem->pModemConfig->Count = 0;
        pModem->pModemConfig->Status = PN_CSTT_GET_STATE;
      }
    }
    break;

    case PN_CSTT_GET_STATE:
    {
      if (strstr(pStr->pData, "OK\r\n"))
      {
        pModem->pModemConfig->Count = 0;
        pModem->pModemConfig->Status = PN_CGREG_GET_STATE;
      }
    }
    break;
/*
    case PN_CGREG_GET_STATE:
    {
      if (strstr(pStr->pData, "OK\r\n"))
      {
        pModem->pModemConfig->Count = 0;
        pModem->pModemConfig->Status = PN_CMGF;
        pModem->pModemConfig->Status = PN_VOID;
      }

    }
    break;
*/

    case PN_CMGF:
    {
    	if (strstr(pStr->pData, "OK\r\n"))
    	{
    		pModem->pModemConfig->Count = 0;
    		pModem->pModemConfig->Status = PN_CMGS;
    	}
    }
    break;

    case PN_CMGS:
    {
    	if (strstr(pStr->pData, "> "))
    	{
    		pModem->pModemConfig->Count = 0;
    		pModem->pModemConfig->Status = PN_SEND_MSG;
    	}
    }

    case PN_SEND_MSG:
    {
    	if (strstr(pStr->pData, "\r\n"))
    	{
    		pModem->pModemConfig->Count = 0;
    		pModem->pModemConfig->Status = PN_READY;
    	}
    }

    case PN_READY:
    case PN_ERROR:
    default:
    break;
  }
}

/// @} PnProtocol
