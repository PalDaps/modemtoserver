/**
  * @file    PN6280.c
  * @author  Dashko P.P. (NIIMA PROGRESS)
  * @brief   Файл с описанием функций API для устройства
  */

#include "modem.h"
#include "assert.h"
#include "stdlib.h"
#include "string.h"
#include "stdbool.h"
#include "progress_server.h"

static void SendToUser(void* pHandle, char* pData, size_t Size);
static void SendToRos(void* pHandle, char* pData, size_t Size);
static void TransmitMsg(void* pHandle, void* pTxData, size_t Size);
static inline void ModemProtocolCallbacksInit(Modem_t* pModem);
static inline ModemPwrSettings_t* ModemPwrSettingsInit(Modem_t* pModem);
static inline ModemConfig_t* ModemConfigInit(Modem_t* pModem);
static inline ModemSock_t* ModemSockInit(Modem_t* pModem);
/**
 * @brief Инициализация PN6280
 * @param[in]  pUartHandle          Указатель на дескриптор интерфейса UART для приема и отправки
 *
 */
Modem_t* ModemInit(UART_HandleTypeDef* pUartHandle, ModemUserCallbacks_t* pCallback)
{
	assert(pUartHandle);

	Modem_t* pModem = fpUtilMalloc(sizeof(Modem_t));

	UartPort_t* pUart = fpUtilMalloc(sizeof(UartPort_t));
	UartPortSoftware_t* pSoftUart = fpUtilMalloc(sizeof(UartPortSoftware_t));
	UartInterfaceSoftware_t* pSoftInterfaceUart = fpUtilMalloc(sizeof(UartInterfaceSoftware_t));

	RingBuffer_t* pPnRingBuffer = fpUtilMalloc(sizeof(RingBuffer_t));
	uint8_t* pBuffer = fpUtilMalloc(PN6280_UART_BUFF_LEN);
	memset(pBuffer, 0x00, PN6280_UART_BUFF_LEN);
	UartPortInit(pUart, pUartHandle);
	UartPortSoftInit(pSoftUart, pUart, 100);

	pModem->pUartSoftInterface = pSoftInterfaceUart;
	UartInterfaceSoftInit(pSoftInterfaceUart, pSoftUart);
	RingBufInit(pPnRingBuffer, pBuffer, PN6280_UART_BUFF_LEN);
	pModem->pPnProtocol = PnProtocolInit((RingBuffer_t*)pPnRingBuffer, (PnTransmit_t)TransmitMsg, pModem);
	pModem->pModemPwrSettings = ModemPwrSettingsInit(pModem);
	pModem->pModemConfig = ModemConfigInit(pModem);
	pModem->pModemSock = ModemSockInit(pModem);
	pModem->pCallbacks = pCallback;
	ModemProtocolCallbacksInit(pModem);
	UartSoftReceive(pModem->pUartSoftInterface, pPnRingBuffer->pBegin, PN6280_UART_BUFF_LEN, true);
	return pModem;
}

static inline ModemPwrSettings_t* ModemPwrSettingsInit(Modem_t* pModem)
{
	ModemPwrSettings_t* pModemPwrSettings = fpUtilMalloc(sizeof(ModemPwrSettings_t));
	Gpio_t* pPwron = (Gpio_t*)malloc(sizeof(Gpio_t));
	Gpio_t* pModemReset = (Gpio_t*)malloc(sizeof(Gpio_t));
	Gpio_t* pWake = (Gpio_t*)malloc(sizeof(Gpio_t));

	GpioInit(pPwron, PN6280_PWRON_PORT, PN6280_PWRON_PIN);
	GpioInit(pModemReset, PN6280_MODEM_RESET_PORT, PN6280_MODEM_RESET_PIN);
	GpioInit(pWake, PN6280_WAKE_PORT, PN6280_WAKE_PIN);
	GpioSetValue(pPwron, 1);
	GpioSetValue(pModemReset, 0);
	GpioSetValue(pWake, 0);

	pModem->pModemPwrSettings = pModemPwrSettings;
	pModem->pModemPwrSettings->pPwron = pPwron;
	pModem->pModemPwrSettings->pModemReset = pModemReset;
	pModem->pModemPwrSettings->pWake = pWake;
	return pModemPwrSettings;
}

static inline void ModemPwrSettingsDeinit(Modem_t* pModem)
{
	free(pModem->pModemPwrSettings->pPwron);
	free(pModem->pModemPwrSettings->pModemReset);
	free(pModem->pModemPwrSettings->pWake);
	fpUtilFree(pModem->pModemPwrSettings);
}

static inline ModemConfig_t* ModemConfigInit(Modem_t* pModem)
{
	ModemConfig_t* pModemConfig = fpUtilMalloc(sizeof(ModemConfig_t));

	pModem->pModemConfig = pModemConfig;
	pModem->pModemConfig->Count = 0;
	pModem->pModemConfig->LastTime = 0;
	pModem->pModemConfig->Sim = SIM_GLONASS;
	pModem->pModemConfig->Status = PN_UNDEF;
	pModem->pModemConfig->Type = MODEM_PN6280;
	return pModemConfig;
}

static inline ModemSock_t* ModemSockInit(Modem_t* pModem)
{
	ModemSock_t* pModemSock = fpUtilMalloc(sizeof(ModemSock_t));

	pModem->pModemSock = pModemSock;

	pModem->pModemSock->Ip[0] = '\0';
	pModem->pModemSock->Port[0] = '\0';
	pModem->pModemSock->Network = modNetNone;
  pModem->pModemSock->Status = modSockClose;
	pModem->pModemSock->Auth = false;
	pModem->pModemSock->Rtk = false;
	pModem->pModemSock->Ppp = false;
	return pModemSock;
}
/**
 * @brief Инициализация коллбеков протокольного уровня
 * @param[out] pModem            Указатель на структуру состояния модема PN6280
 */
static inline void ModemProtocolCallbacksInit(Modem_t* pModem)
{
	PnProtocol_t* pProtocol = (PnProtocol_t*)pModem->pPnProtocol;
	PnCallbacks_t* pCallbacks = pProtocol->Callbacks;

	pCallbacks->Common->fpSendToRos = (void*)SendToRos;
	pCallbacks->Common->fpSendToUsb = (void*)SendToUser;
}

static inline void ModemProtocolCallbacksDeinit(Modem_t* pModem)
{
	PnProtocol_t* pProtocol = (PnProtocol_t*)pModem->pPnProtocol;
	PnCallbacks_t* pCallbacks = pProtocol->Callbacks;

	pCallbacks->Common->fpSendToRos = NULL;
	pCallbacks->Common->fpSendToUsb = NULL;
}
/**
 * @brief Деинициализация PN6280
 * @param[out] pModem            Указатель на структуру состояния модема PN6280
 */
void ModemDeinit(Modem_t* pModem)
{
	assert(pModem);

	UartSoftStopReceive(pModem->pUartSoftInterface);
	ModemProtocolCallbacksDeinit(pModem) ;
	fpUtilFree(pModem->pCallbacks);
	fpUtilFree(pModem->pModemSock);
	fpUtilFree(pModem->pModemConfig);
	ModemPwrSettingsDeinit(pModem);
	fpUtilFree(((PnProtocol_t*)pModem->pPnProtocol)->Parser->pRingBuffer->pBegin);
	free(((PnProtocol_t*)pModem->pPnProtocol)->Parser->pRingBuffer);
	PnProtocolDeInit(pModem->pPnProtocol);
	UartPortSoftDeinit(pModem->pUartSoftInterface->pSoftUart);
	UartInterfaceSoftDeinit(pModem->pUartSoftInterface);
	fpUtilFree(pModem->pUartSoftInterface->pSoftUart);
	fpUtilFree(pModem->pUartSoftInterface);
}
/**
 * @brief Обновление состояния входного массива и обработка данных
 * @param[in] pModem             Указатель на структуру состояния модема PN6280
 */
void ModemProcessing(Modem_t* pModem, DataFlowControl_t* pDataFlowControl)
{
		PnProtocol_t* pPnProtocol = (PnProtocol_t*)pModem->pPnProtocol;
		const size_t WriteIndex = PN6280_UART_BUFF_LEN -__HAL_DMA_GET_COUNTER(pModem->pUartSoftInterface->pSoftUart->pUart->pHandle->hdmarx);
		pPnProtocol->Parser->pWrite = pPnProtocol->Parser->pRingBuffer->pBegin + WriteIndex;
		PnProcess(pPnProtocol, pDataFlowControl);
}
/**
 * @brief       Отправка сообщения на User уровень
 * @param[in]   pHandle       Указатель на модуль ПН6280
 * @param[in]   pTxData       Указатель на данные для передачи
 * @param[in]   Size          Размер данных
 */


static void SendToUser(void* pHandle, char* pData, size_t Size)
{
	Modem_t* pModem = (Modem_t*)pHandle;
	pModem->pCallbacks->fpSendToUser(pData, Size);
}
/**
 * @brief       Отправка сообщения на Device уровень
 * @param[in]   pHandle       Указатель на модуль ПН6280
 * @param[in]   pTxData       Указатель на данные для передачи
 * @param[in]   Size          Размер данных
 */
static void SendToRos(void* pHandle, char* pData, size_t Size)
{
	Modem_t* pModem = (Modem_t*)pHandle;
	pModem->pCallbacks->fpSendToUartRos(pData, Size);
}
/**
 * @brief       Отправка сообщения
 * @param[in]   pHandle       Указатель на параметр возвращаемый с протокольного уровня
 * @param[in]   pTxData       Указатель на данные для передачи
 * @param[in]   Size          Размер данных
 */
static void TransmitMsg(void* pHandle, void* pTxData, size_t Size)
{
  Modem_t* pModem = (Modem_t*)pHandle;
  char* pMsg = (char*)fpUtilMalloc(Size);
  memcpy(pMsg, pTxData, Size);
  UartSoftTransmit(pModem->pUartSoftInterface, pMsg, Size, false, true);
}

void ModemSockConnect(void* pHandle, uint8_t SocketNumber, char* pIpAddr, uint16_t Port)
{
	Modem_t* pModem = (Modem_t*)pHandle;

  char FullCmd[128];
  // "AT+CIPSTART=%d,\"TCP\",\"%s\",\"%d\"\r"
  snprintf(FullCmd, sizeof(FullCmd), "AT+CIPSTART=\"TCP\",\"%s\",\"%d\"\r", pIpAddr, Port);
  String_t Str = StrMakeFromArray(FullCmd, 128);
  ModemSend(pModem, &Str);
  StrClear(&Str);
}

void ModemSockConnectUdp(void* pHandle, uint8_t SocketNumber, char* pIpAddr, uint16_t Port)
{
	Modem_t* pModem = (Modem_t*)pHandle;

  char FullCmd[128];
  snprintf(FullCmd, sizeof(FullCmd), "AT+CIPSTART=%d,\"UDP\",\"%s\",\"%d\"\r", SocketNumber, pIpAddr, Port);
  String_t Str = StrMakeFromArray(FullCmd, 128);
  ModemSend(pModem, &Str);
  StrClear(&Str);
}

void ModemSockDisConnectUdp(void* pHandle, uint8_t SocketNumber)
{
	Modem_t* pModem = (Modem_t*)pHandle;

  char FullCmd[128];
  snprintf(FullCmd, sizeof(FullCmd), "AT+CIPCLOSE=%d\r", SocketNumber);
  String_t Str = StrMakeFromArray(FullCmd, 128);
  ModemSend(pModem, &Str);
  StrClear(&Str);
}

void GsmWriteMultiSocketNonTransporent(void* pHandle, uint8_t SocketNumber, String_t* pStr, SendState_t* State)
{
	Modem_t* pModem = (Modem_t*)pHandle;
  char FullCmd[20];

  while (*State != SEND_STATE_UNDEF)
  {
    switch (*State)
    {
      case SEND_STATE_ATCMD:
      {
        snprintf(FullCmd, sizeof(FullCmd), "AT+CIPSEND=%d\r", pStr->Length);
        String_t Str = StrMakeFromArray(FullCmd, 20);
        ModemSend(pModem, &Str);
        StrClear(&Str);
        *State = SEND_STATE_WAIT_CMD;
      }
      break;

      case SEND_STATE_DATA:
      {

        StrAppendChar(pStr, (char)0x1A);
        ModemSend(pModem, pStr);
        *State = SEND_STATE_WAIT_CMD;
      }
      break;

      default:
      case SEND_STATE_WAIT_CMD:
      case SEND_STATE_WAIT_DATA:
      break;
    }
  }
}

void UpdateGsmSocket(Modem_t* pModem, CommonConnectPoint_t* pPoint, char* pId)
{
  switch (pPoint->Status)
  {
    case SOCKET_STATUS_CLOSE:
    {
      if ((pPoint->Addr[0] != '0') && (pPoint->Port != 0) && (pPoint->Protocol != CONNECT_PROTOCOL_UNDEF))
      {
        if (pPoint->Protocol != CONNECT_PROTOCOL_NTP)
          ModemSockConnect(pModem, pPoint->SockNumber, pPoint->Addr, pPoint->Port);
        else
          ModemSockConnectUdp(pModem, pPoint->SockNumber, pPoint->Addr, pPoint->Port);

        pPoint->Timeout = 0;
        pPoint->Status = SOCKET_STATUS_GSM_WAIT_OPEN;
      }
    }
    break;

    case SOCKET_STATUS_GSM_WAIT_OPEN:
    {
      if (pPoint->Timeout == 149)
        pPoint->Status = SOCKET_STATUS_CLOSE;
      else
        pPoint->Timeout++;
    }
    break;

    case SOCKET_STATUS_GSM_OPEN:
    {
      if ( (pPoint->Auth == true) && (pPoint->Protocol == CONNECT_PROTOCOL_JSON_PROGRESS) )
      {
      	ProgressAuthSend(pModem);
        pPoint->Timeout = 0;
        pPoint->Status = SOCKET_STATUS_GSM_WAIT_AUTH;
      }
      else if ( (pPoint->Auth == true) && (pPoint->Protocol == CONNECT_PROTOCOL_NTRIP) )
      {
     // RequestNtrip(pPoint);
        pPoint->Timeout = 0;
        pPoint->Status = SOCKET_STATUS_GSM_WAIT_AUTH;
      }
      if (pPoint->Protocol == CONNECT_PROTOCOL_NTP)
      {
     // NtpRequestSend();
        pPoint->Timeout = 0;
        pPoint->Status = SOCKET_STATUS_GSM_WAIT_DATA;
      }
    }
    break;

    case SOCKET_STATUS_GSM_WAIT_DATA:
    case SOCKET_STATUS_GSM_WAIT_AUTH:
    {
      if (pPoint->Timeout == 149)
      {
        pPoint->Status = SOCKET_STATUS_GSM_OPEN;
        pPoint->SendState = SEND_STATE_UNDEF;
      }
      else
        pPoint->Timeout++;
    }
    break;

    case SOCKET_STATUS_GSM_SEND_CLOSE:
    {
      ModemSockDisConnectUdp(pModem, pPoint->SockNumber);
      pPoint->Timeout = 0;
      pPoint->Status = SOCKET_STATUS_GSM_WAIT_CLOSE;
    }
    break;

    case SOCKET_STATUS_GSM_WAIT_CLOSE:
    {
      if (pPoint->Timeout == 149)
        pPoint->Status = SOCKET_STATUS_GSM_SEND_CLOSE;
      else
        pPoint->Timeout++;
    }

    default:
    case SOCKET_STATUS_UNDEF:
    case SOCKET_STATUS_GSM_AUTH:
    break;
  }
}
