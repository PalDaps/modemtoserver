/**
 * @file   	pn_protocol.h
 * @author  Dashko P.P. (NIIMA PROGRESS)
 * @brief   Заголовочный файл для протокола модуля PN6280
 *
 */

#ifndef PN_PROTOCOL_H_
#define PN_PROTOCOL_H_

/**
 * @defgroup PnProtocol
 * @version 0.1
 * @brief Описание определений и функций API PnProtocol
 * @{
 *
 */
#include "stdlib.h"
#include "stdbool.h"
#include "util_ring_buffer.h"
#include "uart_driver.h"
#include "uart_driver_stm.h"
#include "gpio_driver_stm.h"
#include "gpio_driver.h"
#include "usb_driver_stm_ex.h"
#include "stm32f4xx_hal.h"
#include "util_ring_buffer.h"
#include "usb_driver_software.h"
#include "usb_driver_freertos.h"
#include "uart_driver_software.h"
#include "uart_driver_freertos.h"
#include "util_heap.h"
#include "util_string.h"
#include "data_flow_control.h"

#define CGMI_CMD_LEN 	  					7  	///< Длина команды AT+CGMI
#define CGMM_CMD_LEN 	  					7		///< Длина команды AT+CGMM
#define CGMR_CMD_LEN 	  					7	  ///< Длина команды AT+CGMR
#define CGSN_CMD_LEN 	  					7	  ///< Длина команды AT+CGSN
#define CSCS_CMD_LEN 	  					7   ///< Длина команды AT+CSCS
#define CIMI_CMD_LEN 	  					7   ///< Длина команды AT+CIMI
#define CSTA_CMD_LEN 	 					 	7	  ///< Длина команды AT+CSTA
#define CMOD_CMD_LEN 	  					7	  ///< Длина команды AT+CMOD
#define CHUP_CMD_LEN 	  					7	  ///< Длина команды AT+CHUP
#define CR_CMD_LEN   	  					5	  ///< Длина команды AT+CR
#define CEER_CMD_LEN 	  					7	  ///< Длина команды AT+CEER
#define CRC_CMD_LEN  	  					6	  ///< Длина команды AT+CRC

#define CFUN_CMD_LEN      				7
#define CMEE_CMD_LEN      				7
#define COPS_CMD_LEN      				7
#define CGREG_CMD_LEN     				8
#define CGPADDR_CMD_LEN  					10
#define CGDCONT_CMD_LEN  					10
#define CSTT_CMD_LEN      				7
#define CIICR_CMD_LEN     				8
#define CGDATA_CMD_LEN    				9
#define CIPMUX_CMD_LEN    				9   ///< Длина команды AT+CIPMUX

#define CPAS_CMD_LEN      				7
#define CSQ_CMD_LEN       				6
#define CPIN_CMD_LEN   	  				7
#define CICCID_CMD_LEN    				9

#define ESIMS_CMD_LEN     				8
#define CNUM_CMD_LEN      				7
#define ECUSD_CMD_LEN     				8
#define ESIMINFO_CMD_LEN 					11
#define ESPN_CMD_LEN      				7

#define TCPIP_ANS_LEN 			 			8

/**
 * @brief Команды
 */
typedef enum
{
  CMD_VOID,				///<
  СMD_CGMI,  			///<
  CMD_CPAS,     	///<
  CMD_ESIMS,			///<
  CMD_CNUM,				///<
  CMD_ECUSD,    	///<
  CMD_ESIMINFO,		///<
  CMD_ESPN,				///<
  CMD_CSQ,      	///<
  CMD_CICCID,   	///<
  CMD_CPIN,     	///<
  CMD_CIMI,  			///<
  СMD_CFUN,  			///<
  СMD_CMEE,  			///<
  CMD_COPS,     	///<
  СMD_CGREG, 			///<
  СMD_CGPADDR,		///<
  СMD_CGDCONT,		///<
  СMD_CSTT,  			///<
  СMD_CIICR, 			///<
  СMD_CGDATA,			///<
  СMD_CGMM,  			///<
  СMD_CGMR,  			///<
  СMD_CGSN,  			///<
  СMD_CSCS,  			///<
  СMD_CSTA,  			///<
  CMD_CMOD,  			///<
  CMD_CHUP,  			///<
  CMD_CR,    			///<
  CMD_CEER,  			///<
  CMD_CIPMUX,			///<
  CMD_CRC,   			///<
  CMD_AT,					///<
  CMD_TCPIP     	///<
} PnCommand_t;
/**
 * @brief Тип команды
 */
typedef enum
{
	// AT+{CMD}=?
	CMD_GET_PARAMS, ///< Тип команды для просмотра какие параметры поддерживает данная команда
	// AT+{CMD}?
	CMD_GET_STATE,  ///< Тип команды для просмотра состояния комады
	// AT+{CMD}={PARAMS}
	CMD_SET_PARAMS, ///< Тип команды для утсановки значений
	// AT+{CMD}
	СMD_DEFAULT,    ///< Тип команды для установки или запроса данных
	// Заглушка
	CMD_VOID_TYPE,  ///< Несуществуюший тип команды
} PnCommandType_t;
/**
 * @brief Структура для команды
 */
/**
 * @brief
 */
typedef enum
{
  PN_CMD_READ,							///< Обработка команды
  PN_SERV_SYMBS,            ///< Чтение служебных символов
  PN_ANSWER_READY,          ///< Ответ получен
  PN_ANSWER_VOID,						///< Ответа нетs
} PnParserState_t;

/**
 * @brief Спецификаторы
 */
typedef enum
{
  PN_COMMAND = 0,       	///< Команда
} PnSpecifier_t;

/**
 * @brief Структура для кольцевого буфера с необходимыми параметрами
 */
typedef struct
{
  RingBuffer_t*       						pRingBuffer;  	///< Кольцевой буфер
  char*            	  						pWrite;       	///< Курсор записи
  char*           	  						pRead;        	///< Курсор чтения
  uint8_t*            						pEnd;         	///< Курсор конца сообщения
  uint8_t*            						pStart;					///< Курсор начала сообщения
  uint8_t*            						pStartCmd;    	///< Курсор начала последнего отправленного сообщения
  PnParserState_t 	  						State;        	///< Текущее состояние поиска
  PnCommand_t         						Command;      	///< Текущая команда
  PnCommandType_t     						CommandType;  	///< Текущий тип команды
  uint16_t            						Length;       	///< Длина сообщения
} PnParser_t;
/// @brief Прототип функции передачи данных от устройства
typedef void (*PnTransmit_t)(void* pHandle, const char* pData, uint8_t Size);
/// @brief Прототип функции передачи данных на РОС
typedef void (*ForwardToRos)(void* pHandle, char* pData, size_t Size);
/// @brief Прототип функции передачи данных на USB
typedef void (*ForwardToUsb)(void* pHandle, char* pData, size_t Size);
/**
 * @brief Функции передачи
 */
typedef struct
{
  PnTransmit_t  									fpTransmit;   ///< Отправка сообщения
  ForwardToRos 										fpSendToRos;	///< Отправка сообщения на РОС
  ForwardToUsb 										fpSendToUsb;  ///< Отправка сообщения на USB
} PnCommonCallbacks_t;
/**
 * @brief Вспомогательные параметры
 */
typedef struct
{
  void* 													pCommon;  		///< Указатель на структуру User Interface
} PnUserArguments_t;

/**
 * @brief Все обратные вызовы протокола
 */
typedef struct
{
  PnUserArguments_t* 							UserArgument; ///< Вспомогательные параметры
  PnCommonCallbacks_t* 						Common;       ///< Функции передачи
} PnCallbacks_t;

/**
 * @brief Обезличенный протокол модуля PN6280
 */
typedef struct
{
  PnParser_t*        							Parser;       ///< Кольцевой буфер
  PnCallbacks_t*     							Callbacks;    ///< Callbacks
} PnProtocol_t;
/**
 * @brief Функции для создания команд
 */
PnProtocol_t* PnProtocolInit(RingBuffer_t* pRingBuffer, PnTransmit_t fpTransmitMsg, void* pHandle);
void PnProtocolDeInit(PnProtocol_t* pPnProtocol);
void PnProcess(PnProtocol_t* pPnProtocol, DataFlowControl_t* pDataFlowControl);
void SendATcmd(PnProtocol_t* pPnProtocol, const char* pCmd);

///@} PnProtocol

#endif /* PN_PROTOCOL_H_ */
