/**
 * @file   	pn_protocol.h
 * @author  Dashko P.P. (NIIMA PROGRESS)
 * @brief   Файл с описанием для протокола модуля PN6280
 */

/// @addtogroup PnProtocol
/// @{

#include "pn_protocol.h"
#include "stdio.h"
#include "string.h"
#include "util_string.h"
#include "modem.h"

/**
 * @brief     Инициализация протокола для модема PN6280
 * @param[in] RingBuffer    - кольцевой буфер, указывающий на начало и конец буфера интерфейса
 * @param[in] fpTransmitMsg - указатель на функцию передачи по интерфейсу
 * @param[in] pHandle       - указатель на UserInterface структуру, куда входит экземпляр pPnProtocol
 * @return    pPnProtocol   - указатель на структуру протокола модема PN6280
 *
 */
PnProtocol_t* PnProtocolInit(RingBuffer_t* pRingBuffer, PnTransmit_t fpTransmitMsg, void* pHandle)
{
	assert(fpTransmitMsg);
	assert(pHandle);
	PnProtocol_t* pPnProtocol = (PnProtocol_t*)fpUtilMalloc(sizeof(PnProtocol_t));
	PnCallbacks_t* pPnCallbacks = (PnCallbacks_t*)fpUtilMalloc(sizeof(PnProtocol_t));
	PnCommonCallbacks_t* pPnCommonCallbacks = (PnCommonCallbacks_t*)fpUtilMalloc((sizeof(PnCommonCallbacks_t)));
	PnUserArguments_t* pPnsUserArgument = (PnUserArguments_t*)fpUtilMalloc((sizeof(PnUserArguments_t)));
	PnParser_t* pPnParser = (PnParser_t*)fpUtilMalloc(sizeof(PnParser_t));
	pPnProtocol->Parser = pPnParser;
	pPnProtocol->Parser->pRingBuffer = pRingBuffer;
	pPnProtocol->Parser->Length = 0;
	pPnProtocol->Parser->pRead = (char *)pRingBuffer->pBegin;
	pPnProtocol->Parser->pWrite = (char *)pRingBuffer->pEnd;
	pPnProtocol->Parser->pStartCmd = NULL;
	pPnProtocol->Parser->pStart = NULL;
	pPnProtocol->Parser->pEnd = NULL;
	pPnProtocol->Callbacks = pPnCallbacks;
	pPnProtocol->Callbacks->Common = pPnCommonCallbacks;
	pPnProtocol->Callbacks->UserArgument = pPnsUserArgument;
	pPnProtocol->Callbacks->Common->fpTransmit = fpTransmitMsg;
	pPnProtocol->Callbacks->UserArgument->pCommon = pHandle;
	return pPnProtocol;
}
/**
 * @brief     Деинициализация протокола модема PN6280
 * @param[in] pPnProtocol - указатель на структуру протокола модема PN6280
 * @return    none
 */
void PnProtocolDeInit(PnProtocol_t* pPnProtocol)
{
	assert(pPnProtocol);
	fpUtilFree(pPnProtocol->Callbacks->Common);
	fpUtilFree(pPnProtocol->Callbacks->UserArgument);
	fpUtilFree(pPnProtocol->Callbacks);
	fpUtilFree(pPnProtocol->Parser);
	fpUtilFree(pPnProtocol);
}
/**
 *
 * @brief     Функция для определения команды
 * @param[in] pCmd - указатель на полную команду
 *
 */
PnCommand_t DefineCmd(PnProtocol_t* pPnProtocol, const char* pCmd)
{

	PnParser_t* pParser = pPnProtocol->Parser;
    if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "AT+CGMI", CGMI_CMD_LEN))
        return СMD_CGMI;

    if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "AT+CIMI", CIMI_CMD_LEN))
    	return CMD_CIMI;

    if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "AT+CFUN", CFUN_CMD_LEN))
    	return СMD_CFUN;

    if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "AT+CMEE", CMEE_CMD_LEN))
    	return СMD_CMEE;

    if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "AT+COPS", COPS_CMD_LEN))
    	return CMD_COPS;

    if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "AT+CGREG", CGREG_CMD_LEN))
    	return СMD_CGREG;

    if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "AT+CGPADDR", CGPADDR_CMD_LEN))
    	return СMD_CGPADDR;

    if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "AT+CGDCONT", CGDCONT_CMD_LEN))
    	return СMD_CGDCONT;

    if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "AT+CSTT", CSTT_CMD_LEN))
    	return СMD_CSTT;

    if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "AT+CIICR", CIICR_CMD_LEN))
    	return СMD_CIICR;

    if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "AT+CGDATA", CGDATA_CMD_LEN))
    	return СMD_CGDATA;

    if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "AT+CIPMUX", CIPMUX_CMD_LEN))
    	return CMD_CIPMUX;

    if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "AT+CGMM", CGMM_CMD_LEN))
        return СMD_CGMM;

    if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "AT+CGMR", CGMR_CMD_LEN))
        return СMD_CGMR;

    if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "AT+CPAS", CPAS_CMD_LEN))
    	return CMD_CPAS;

    if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "AT+CSQ", CSQ_CMD_LEN))
    	return CMD_CSQ;

    if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "AT+CPIN", CPIN_CMD_LEN))
    	return CMD_CPIN;

    if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "AT+CICCID", CICCID_CMD_LEN))
    	return CMD_CICCID;

    if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "AT+ESIMS", ESIMS_CMD_LEN))
        return CMD_ESIMS;

    if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "AT+CNUM", CNUM_CMD_LEN))
        return CMD_CNUM;

    if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "AT+ECUSD", ECUSD_CMD_LEN))
        return CMD_ECUSD;

    if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "AT+ESIMINFO", ESIMINFO_CMD_LEN))
        return CMD_ESIMINFO;

    if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "AT+ESPN", ESPN_CMD_LEN))
        return CMD_ESPN;

    if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "AT", 2))
        return CMD_AT;

    if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "TCPIP OK", TCPIP_ANS_LEN))
    	return CMD_TCPIP;
    return CMD_VOID;
}
/**
 * @brief     Функция для определения типа команды
 * @param[in] pCmd - указатель на полную команду
 *
 */
PnCommandType_t DefineCmdType(PnProtocol_t* pPnProtocol)
{
	PnParser_t* pParser = pPnProtocol->Parser;
	if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "=?", 2))
		return CMD_GET_PARAMS;
	else if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "?", 1))
		return CMD_GET_STATE;
	else if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "=", 1))
		return CMD_SET_PARAMS;
	else if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "AT+", 3))
		return СMD_DEFAULT;
	else return CMD_VOID_TYPE;
}
/**
 *
 * @brief     Функция отправки сообщения
 * @details
 * @param[in] pPnProtocol - указатель на интерфейсную структуру
 * @param[in] pMessage    - указатель на буфер с сообщением (обернутым в служебную информацию)
 * @param[in] Size        - размер сообщения с учетом служебной информации и поля CRC
 * @return    Результат отправки
 *
 */
static void ProcessMessage(PnProtocol_t* pPnProtocol, const char* pMsg, DataFlowControl_t* pDataFlowControl)
{
	PnParser_t* pParser = pPnProtocol->Parser;

	pParser->Command = DefineCmd(pPnProtocol, pMsg);
	pParser->CommandType = DefineCmdType(pPnProtocol);
	Modem_t* pModem = (Modem_t*)pPnProtocol->Callbacks->UserArgument->pCommon;

	PnCommand_t State = pParser->Command;
	String_t Str = StrMakeFromRingBuffer(pParser->pRingBuffer, pParser->pRead, pParser->pWrite);

	if (pModem->pModemConfig->Status != PN_READY)
		ParserModemAnswer(pPnProtocol->Callbacks->UserArgument->pCommon, &Str);
	else
	{
	uint8_t tmpSocketNumber = 0;
		if (strstr(Str.pData, "CONNECT OK\r\n"))
		{
		 sscanf(Str.pData, "%hhd,CONNECT OK\r\n", &tmpSocketNumber);

			if (tmpSocketNumber == 0)
				pDataFlowControl->CmdPoint.Status = SOCKET_STATUS_GSM_OPEN;
			/*
			else if (tmpSocketNumber == 1)
				pDataFlowControl->RtkPoint.Status = SOCKET_STATUS_GSM_OPEN;
			else if (tmpSocketNumber == 2)
							pDataFlowControl->PppPoint.Status = SOCKET_STATUS_GSM_OPEN;
			else if (tmpSocketNumber == 3)
				pDataFlowControl->NtpPoint.Status = SOCKET_STATUS_GSM_OPEN;
			*/
		}
		else if (strstr(Str.pData, "ALREADY CONNECT\r\n"))
		{
			sscanf(Str.pData, "%hhd,ALREADY CONNECT\r\n", &tmpSocketNumber);
			if (tmpSocketNumber == 0)
				pDataFlowControl->CmdPoint.Status = SOCKET_STATUS_GSM_OPEN;
			/*
			else if (tmpSocketNumber == 1)
				pDataFlowControl->RtkPoint.Status = SOCKET_STATUS_GSM_OPEN;
			else if (tmpSocketNumber == 2)
							pDataFlowControl->PppPoint.Status = SOCKET_STATUS_GSM_OPEN;
			else if (tmpSocketNumber == 3)
				pDataFlowControl->NtpPoint.Status = SOCKET_STATUS_GSM_OPEN;
			*/
		}
		else if (strstr(Str.pData, "SEND OK\r\n"))
		{
			if (pDataFlowControl->CmdPoint.SendState == SEND_STATE_WAIT_DATA)
				pDataFlowControl->CmdPoint.SendState = SEND_STATE_UNDEF;
			/*
			else if (pDataFlowControl->RtkPoint.SendState == SEND_STATE_WAIT_DATA)
				pDataFlowControl->RtkPoint.SendState = SEND_STATE_UNDEF;
			else if (pDataFlowControl->PppPoint.SendState == SEND_STATE_WAIT_DATA)
				pDataFlowControl->PppPoint.SendState = SEND_STATE_UNDEF;
			else if (pDataFlowControl->NtpPoint.SendState == SEND_STATE_WAIT_DATA)
				pDataFlowControl->NtpPoint.SendState = SEND_STATE_UNDEF;
			*/
		}
		else if (strstr(Str.pData, "ERROR\r\n"))
		{
			if (pDataFlowControl->CmdPoint.SendState == SEND_STATE_WAIT_DATA)
				pDataFlowControl->CmdPoint.SendState = SEND_STATE_DATA;
			/*
			else if (pDataFlowControl->RtkPoint.SendState == SEND_STATE_WAIT_DATA)
				pDataFlowControl->RtkPoint.SendState = SEND_STATE_DATA;
			else if (pDataFlowControl->PppPoint.SendState == SEND_STATE_WAIT_DATA)
				pDataFlowControl->PppPoint.SendState = SEND_STATE_DATA;
			else if (pDataFlowControl->NtpPoint.SendState == SEND_STATE_WAIT_DATA)
				pDataFlowControl->NtpPoint.SendState = SEND_STATE_DATA;
			*/
		}
		if (strstr(Str.pData, "CLOSE OK\r\n"))
		{
			sscanf(Str.pData, "%hhd,CLOSE OK\r\n", &tmpSocketNumber);

			if (tmpSocketNumber == 0)
				pDataFlowControl->CmdPoint.Status = SOCKET_STATUS_CLOSE;
			/*
			else if (tmpSocketNumber == 1)
				pDataFlowControl->RtkPoint.Status = SOCKET_STATUS_CLOSE;
			else if (tmpSocketNumber == 2)
				pDataFlowControl->PppPoint.Status = SOCKET_STATUS_CLOSE;
			else if (tmpSocketNumber == 3)
				pDataFlowControl->NtpPoint.Status = SOCKET_STATUS_CLOSE;
			*/
		}
	}
	if (Str.Length)
	{
		pPnProtocol->Callbacks->Common->fpSendToUsb(pPnProtocol->Callbacks->UserArgument->pCommon, Str.pData, Str.Length);
		osDelay(1);
	}
	StrClear(&Str);
	switch(State)
	{
		case CMD_AT:
			pPnProtocol->Parser->State = PN_ANSWER_READY;
			break;
		case CMD_TCPIP:
			pPnProtocol->Parser->State = PN_ANSWER_READY;
			break;
		case СMD_CGMI:
			if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "OK", 2))
				pPnProtocol->Parser->State = PN_ANSWER_READY;
			else
				pPnProtocol->Parser->State = PN_ANSWER_VOID;
			break;
		case CMD_CIMI:
			if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "OK", 2))
				pPnProtocol->Parser->State = PN_ANSWER_READY;
			else
				pPnProtocol->Parser->State = PN_ANSWER_VOID;
			break;
		case СMD_CFUN:
			if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "OK", 2))
				pPnProtocol->Parser->State = PN_ANSWER_READY;
			else
				pPnProtocol->Parser->State = PN_ANSWER_VOID;
			break;
		case СMD_CMEE:
			if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "OK", 2))
				pPnProtocol->Parser->State = PN_ANSWER_READY;
			else
				pPnProtocol->Parser->State = PN_ANSWER_VOID;
			break;
		case CMD_COPS:
			if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "OK", 2))
				pPnProtocol->Parser->State = PN_ANSWER_READY;
			else
				pPnProtocol->Parser->State = PN_ANSWER_VOID;
			break;
		case СMD_CGREG:
			if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "OK", 2))
				pPnProtocol->Parser->State = PN_ANSWER_READY;
			else
				pPnProtocol->Parser->State = PN_ANSWER_VOID;
			break;
		case СMD_CGPADDR:
			if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "OK", 2))
				pPnProtocol->Parser->State = PN_ANSWER_READY;
			else
				pPnProtocol->Parser->State = PN_ANSWER_VOID;
			break;
		case СMD_CGDCONT:
			if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "OK", 2))
				pPnProtocol->Parser->State = PN_ANSWER_READY;
			else
				pPnProtocol->Parser->State = PN_ANSWER_VOID;
			break;
		case СMD_CSTT:
			if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "OK", 2))
				pPnProtocol->Parser->State = PN_ANSWER_READY;
			else if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "ERROR", 5))
				pPnProtocol->Parser->State = PN_ANSWER_READY;
			else
				pPnProtocol->Parser->State = PN_ANSWER_VOID;
			break;
		case СMD_CIICR:
			if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "OK", 2))
				pPnProtocol->Parser->State = PN_ANSWER_READY;
			else if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "ERROR", 5))
				pPnProtocol->Parser->State = PN_ANSWER_READY;
			else
				pPnProtocol->Parser->State = PN_ANSWER_VOID;
			break;
		case СMD_CGDATA:
			if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "OK", 2))
				pPnProtocol->Parser->State = PN_ANSWER_READY;
			else if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "ERROR", 5))
				pPnProtocol->Parser->State = PN_ANSWER_READY;
			else
				pPnProtocol->Parser->State = PN_ANSWER_VOID;
			break;
		case CMD_CIPMUX:
			if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "OK", 2))
				pPnProtocol->Parser->State = PN_ANSWER_READY;
			else
				pPnProtocol->Parser->State = PN_ANSWER_VOID;
			break;
		case СMD_CGMM:
			if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "OK", 2))
				pPnProtocol->Parser->State = PN_ANSWER_READY;
			else
				pPnProtocol->Parser->State = PN_ANSWER_VOID;
			break;
		case СMD_CGMR:
			if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "OK", 2))
				pPnProtocol->Parser->State = PN_ANSWER_READY;
			else
				pPnProtocol->Parser->State = PN_ANSWER_VOID;
			break;
		case CMD_CPAS:
			if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "OK", 2))
				pPnProtocol->Parser->State = PN_ANSWER_READY;
			else
				pPnProtocol->Parser->State = PN_ANSWER_VOID;
			break;
		case CMD_CSQ:
			if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "OK", 2))
				pPnProtocol->Parser->State = PN_ANSWER_READY;
			else
				pPnProtocol->Parser->State = PN_ANSWER_VOID;
			break;
		case CMD_CICCID:
			if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "OK", 2))
				pPnProtocol->Parser->State = PN_ANSWER_READY;
			else
				pPnProtocol->Parser->State = PN_ANSWER_VOID;
			break;
		case CMD_CPIN:
			if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "OK", 2))
				pPnProtocol->Parser->State = PN_ANSWER_READY;
			else
				pPnProtocol->Parser->State = PN_ANSWER_VOID;
			break;
		case CMD_ESIMS:
			if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "OK", 2))
				pPnProtocol->Parser->State = PN_ANSWER_READY;
			else
				pPnProtocol->Parser->State = PN_ANSWER_VOID;
			break;
		case CMD_CNUM:
			if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "OK", 2))
				pPnProtocol->Parser->State = PN_ANSWER_READY;
			else if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "ERROR", 5))
				pPnProtocol->Parser->State = PN_ANSWER_READY;
			else
				pPnProtocol->Parser->State = PN_ANSWER_VOID;
			break;
		case CMD_ECUSD:
			if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "OK", 2))
				pPnProtocol->Parser->State = PN_ANSWER_READY;
			else
				pPnProtocol->Parser->State = PN_ANSWER_VOID;
			break;
		case CMD_ESIMINFO:
			if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "OK", 2))
				pPnProtocol->Parser->State = PN_ANSWER_READY;
			else if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "ERROR", 5))
				pPnProtocol->Parser->State = PN_ANSWER_READY;
			else
				pPnProtocol->Parser->State = PN_ANSWER_VOID;
			break;
		case CMD_ESPN:
			if (RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "OK", 2))
				pPnProtocol->Parser->State = PN_ANSWER_READY;
			else
				pPnProtocol->Parser->State = PN_ANSWER_VOID;
			break;
		case CMD_CR:
			break;
		case CMD_VOID:
			break;
		case СMD_CGSN:
			break;
		case СMD_CSCS:
			break;
		case СMD_CSTA:
			break;
		case CMD_CMOD:
			break;
		case CMD_CHUP:
			break;
		case CMD_CEER:
			break;
		case CMD_CRC:
			break;
	}
}
/**
 * @brief     Функция разбора команд и запросов формата протокола
 * @details
 * @param[in] pPnProtocol - указатель на интерфейсную структуру
 */
void PnProcess(PnProtocol_t* pPnProtocol, DataFlowControl_t* pDataFlowControl)
{
  assert(pPnProtocol);

  PnParser_t* pParser = pPnProtocol->Parser;
  if (pParser->pRead != pParser->pWrite)
  {

 	  char* pToOk = RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "OK", 2);
	  char* pToTcpIpOk = RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "TCPIP OK", 8);
	  char* pError = RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "ERROR", 5);
	  char* pCgReg = RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "+CGREG", 6);
	  char* pSocketEndSend = RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "SEND OK", 7);
	  char* pSocketClose = RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "CLOSED", 6);
	  char* pSocketStartSend = RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, ">", 1);
	  char* pEndJsonMessage = RingBufFind(pParser->pRingBuffer, pParser->pRead, pParser->pWrite, "}", 1);

	  if (pToOk || pError || pCgReg || pToTcpIpOk || pSocketClose || pEndJsonMessage || pSocketEndSend)
	  {
		  ProcessMessage(pPnProtocol, pToOk, pDataFlowControl);
		  pParser->pRead = pParser->pWrite;
	  }
	  else
	  {
		  pPnProtocol->Parser->State = PN_ANSWER_VOID;
	  }
	  if (pSocketStartSend)
		{
      if (pDataFlowControl->CmdPoint.SendState == SEND_STATE_WAIT_CMD)
      {
      	pDataFlowControl->CmdPoint.SendState = SEND_STATE_DATA;
      }
      	String_t Str = StrMakeFromRingBuffer(pParser->pRingBuffer, pParser->pRead, pParser->pWrite);
      	osDelay(10);
      	pPnProtocol->Callbacks->Common->fpSendToUsb(pPnProtocol->Callbacks->UserArgument->pCommon, Str.pData, Str.Length);
      	StrClear(&Str);
      	pParser->pRead = pParser->pWrite;
		}
	  if (pSocketEndSend)
	  {
	  	pDataFlowControl->CmdPoint.Status = SOCKET_STATUS_UNDEF;
	  }
	  if (pSocketClose)
		{
	  	pDataFlowControl->CmdPoint.Status = SOCKET_STATUS_CLOSE;
		}
  }
}

/// @} PnProtocol
