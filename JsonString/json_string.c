#include "json_string.h"

char* GenProgressAuthStr(char const* pId)
{
  size_t StrSize = JSON_AUTH_MSG_SIZE + 1;
  char* pStr = (char*)fpUtilMalloc(StrSize);
  snprintf(pStr, StrSize, "{\"auth\":\"%11s\",\"sub\":\"LSN/%11s/up\"}", pId, pId);
  return pStr;
}

char* GenServiceInformationStr(const char* sourceDeviceId, const char* payload)
{

	size_t StrSize = 128;
	char* pStr = (char*)fpUtilMalloc(StrSize);

	if (pStr == NULL)
			return NULL;

	snprintf(pStr, StrSize,
			"{\"protocol\":\"service_information\", \"lsncmd\":\"00\", \"sourceDeviceId\":\"%s\", \"payload\":\"%s\"}",
			sourceDeviceId, payload
	);

	return pStr;
}

String_t MakeJsonStr(char const* pStr, size_t const Size)
{
  size_t CntEscape = 0;

  for (size_t Cnt = 0; Cnt < Size; ++Cnt)
  {
    char Symb = pStr[Cnt];

    if (Symb == '\r' || Symb == '\n' || Symb == '"')
      ++CntEscape;
  }

  size_t NewSize = Size + CntEscape + 1;
  char* pOut = fpUtilMalloc(NewSize);
  String_t JsonString;

  if (pOut)
  {
    size_t JsonStrSize = 0;

    for (size_t Cnt = 0; Cnt < Size; ++Cnt)
    {
      char Symb = pStr[Cnt];

      if (Symb == '\n')
      {
        pOut[JsonStrSize++] = '\\';
        pOut[JsonStrSize++] = 'n';
        continue;
      }

      if (Symb == '\r')
      {
        pOut[JsonStrSize++] = '\\';
        pOut[JsonStrSize++] = 'r';
        continue;
      }
      if (Symb == '"')
      {
      	pOut[JsonStrSize++] = '\\';
      	pOut[JsonStrSize++] = '"';
      	continue;
      }

      pOut[JsonStrSize++] = Symb;
    }

    pOut[JsonStrSize] = '\0';

    JsonString = StrMakeFormCStr(pOut);
  }
  else
  {
    JsonString = StrMake();
  }

  fpUtilFree(pOut);
  return JsonString;
}
