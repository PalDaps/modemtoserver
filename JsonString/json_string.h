#ifndef INC_JSON_STRING_H_
#define INC_JSON_STRING_H_

#include "string.h"
#include "stdio.h"
#include "util_string.h"
#include "util_heap.h"

#define JSON_AUTH_MSG_SIZE        49
#define JSON_DATA_MSG_WARP_SIZE   43

char* GenProgressAuthStr(char const* pId);
char* GenProgressDataStr(char const* pId, String_t* pStr);
String_t MakeJsonStr(char const* pStr, size_t const Size);


#endif /* INC_JSON_STRING_H_ */
