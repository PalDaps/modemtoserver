#include "device_protocol.h"
#include "util_string.h"

DeviceProtocol_t* DeviceProtocolInit(void* pHandle)
{
	DeviceProtocol_t* pProtocol = (DeviceProtocol_t*)fpUtilMalloc(sizeof(DeviceProtocol_t));
	RingBuffer_t* pUartRingBuf = malloc(sizeof(RingBuffer_t));

	uint8_t* pUartBuffer = fpUtilMalloc(UART_BUFF_LEN);
	assert(pUartBuffer);
	memset(pUartBuffer, 0x00, UART_BUFF_LEN);

	RingBufInit(pUartRingBuf, pUartBuffer, UART_BUFF_LEN);

	pProtocol->pUartParser = UartParserInit(pUartRingBuf);
	pProtocol->pHandle = pHandle;

	return pProtocol;
}

void DeviceProtocolDeinit(DeviceProtocol_t* pProtocol)
{

	UartParserDeinit(pProtocol->pUartParser);
	fpUtilFree(pProtocol);
}

UartParser_t* UartParserInit(RingBuffer_t* pRingBuffer)
{
	assert(pRingBuffer);

	UartParser_t* pUartParser = (UartParser_t*)fpUtilMalloc(sizeof(UartParser_t));
	pUartParser->pRingBuffer = pRingBuffer;
	pUartParser->Length = 0;
	pUartParser->pRead = pRingBuffer->pBegin;
	pUartParser->pWrite = pRingBuffer->pEnd;

	return pUartParser;
}

void UartParserDeinit(UartParser_t* pUartParser)
{
	assert(pUartParser);

	fpUtilFree(pUartParser->pRingBuffer->pBegin);
	free(pUartParser->pRingBuffer);
	fpUtilFree(pUartParser);
}

void DeviceParserProcessing(DeviceProtocol_t* pProtocol)
{
	if (pProtocol->pUartParser->pRead != pProtocol->pUartParser->pWrite)
	{
		String_t str = StrMakeFromRingBuffer(pProtocol->pUartParser->pRingBuffer, pProtocol->pUartParser->pRead, pProtocol->pUartParser->pWrite);
		if (str.Length)
			pProtocol->fpForward(pProtocol->pHandle, str.pData, str.Length);

		StrClear(&str);
		pProtocol->pUartParser->pRead = pProtocol->pUartParser->pWrite;
	}
}

