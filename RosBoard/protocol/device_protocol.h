#ifndef PARSER_H_
#define PARSER_H_

#include "uart_driver.h"
#include "uart_driver_stm.h"
#include "gpio_driver_stm.h"
#include "gpio_driver.h"
#include "usb_driver_stm_ex.h"
#include "stm32f4xx_hal.h"
#include "util_ring_buffer.h"
#include "usb_driver_software.h"
#include "usb_driver_freertos.h"
#include "uart_driver_software.h"
#include "uart_driver_freertos.h"
#include "util_heap.h"

#define UART_BUFF_LEN         		 10240

typedef void (*TransmitToUsb)(void* pHandle, char* pData, size_t Size);
typedef void (*Transmit)(void* pHandle, const uint8_t* pData, size_t Size);

typedef struct
{
	RingBuffer_t* pRingBuffer;
	char* pWrite;
	char* pRead;
	uint16_t Length;
} UartParser_t;

typedef struct
{
	TransmitToUsb fpForward;
	Transmit fpTransmit;
	UartParser_t* pUartParser;
	void* pHandle;
} DeviceProtocol_t;

DeviceProtocol_t* DeviceProtocolInit(void* pHandle);
void DeviceProtocolDeinit(DeviceProtocol_t* pProtocol);

UartParser_t* UartParserInit(RingBuffer_t* pRingBuffer);
void UartParserDeinit(UartParser_t* pUartParser);

void DeviceParserProcessing(DeviceProtocol_t* pProtocol);



#endif /* PARSER_H_ */

