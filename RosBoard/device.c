#include "device.h"

#include "assert.h"
#include "stdlib.h"
#include "string.h"
#include "stdbool.h"
#include "util_string.h"

static inline void DefaultInitCallbacks(Device_t* pDevice);
static void SendToUser(void* pHandle, char* pData, size_t Size);
static void TransmitMsg(void* pHandle, void* pTxData, size_t Size);

Device_t* DeviceInit(UART_HandleTypeDef* pUartHandle, DeviceCallback_t* pCallBack)
{
	assert(pUartHandle);

	Device_t* pDevice = (Device_t*)fpUtilMalloc(sizeof(Device_t));
	UartPort_t* pUart = fpUtilMalloc(sizeof(UartPort_t));
	UartPortSoftware_t* pSoftUart = fpUtilMalloc(sizeof(UartPortSoftware_t));
	UartInterfaceSoftware_t* pSoftInterfaceUart = fpUtilMalloc(sizeof(UartInterfaceSoftware_t));

	UartPortInit(pUart, pUartHandle);
	UartPortSoftInit(pSoftUart, pUart, 100);
	pDevice->pUartSoftInterface = pSoftInterfaceUart;
	UartInterfaceSoftInit(pSoftInterfaceUart, pSoftUart);

	pDevice->pProtocol = DeviceProtocolInit(pDevice);
	pDevice->pCallback = pCallBack;
	DefaultInitCallbacks(pDevice);
	UartSoftReceive(pDevice->pUartSoftInterface, ((DeviceProtocol_t*)pDevice->pProtocol)->pUartParser->pRingBuffer->pBegin, UART_BUFF_LEN, true);

	return pDevice;
}

static inline void DefaultInitCallbacks(Device_t* pDevice)
{
	DeviceProtocol_t* pProtocol = (DeviceProtocol_t*)pDevice->pProtocol;

	pProtocol->fpForward = SendToUser;
	pProtocol->fpTransmit = (void*)TransmitMsg;
}

void DeviceDeinit(Device_t* pDevice)
{
	assert(pDevice);

	UartSoftStopReceive(pDevice->pUartSoftInterface);
	UartPortDeinit(pDevice->pUartInterface->pPort);
	UartPortSoftDeinit(pDevice->pUartSoftInterface->pSoftUart);
	UartInterfaceSoftDeinit(pDevice->pUartSoftInterface);
	DeviceProtocolDeinit(pDevice->pProtocol);

	fpUtilFree(pDevice->pUartInterface->pPort);
	fpUtilFree(pDevice->pUartSoftInterface->pSoftUart);
	fpUtilFree(pDevice->pUartSoftInterface);

	fpUtilFree(pDevice);
}

void DeviceProcessing(Device_t* pDevice)
{
	DeviceProtocol_t* pProtocol =(DeviceProtocol_t*)pDevice->pProtocol;

	const size_t WriteIndex = UART_BUFF_LEN - __HAL_DMA_GET_COUNTER(pDevice->pUartSoftInterface->pSoftUart->pUart->pHandle->hdmarx);
	pProtocol->pUartParser->pWrite = pProtocol->pUartParser->pRingBuffer->pBegin + WriteIndex;
	DeviceParserProcessing(pProtocol);
}

static void SendToUser(void* pHandle, char* pData, size_t Size)
{
	Device_t* pDevice = (Device_t*)pHandle;

    pDevice->pCallback->fpSendToUser(pData, Size, pDevice->pUserDevice);
}

static void TransmitMsg(void* pHandle, void* pTxData, size_t Size)
{
  Device_t* pDevice = (Device_t*)pHandle;
  char* pMsg = (char*)fpUtilMalloc(Size);
  memcpy(pMsg, pTxData, Size);
  UartSoftTransmit(pDevice->pUartSoftInterface, pMsg, Size, false, true);
}



