#ifndef DEVICE_H_
#define DEVICE_H_

#include "device_protocol.h"

typedef void (*TransmitUser)(const char* pMsg, size_t Size, void* pUserDevice);

typedef struct
{
	TransmitUser fpSendToUser;
} DeviceCallback_t;

typedef struct
{
	void* pProtocol;
	void* pUserDevice;
	DeviceCallback_t* pCallback;
	UartInterface_t* pUartInterface;
	UartInterfaceSoftware_t* pUartSoftInterface;
} Device_t;


Device_t* DeviceInit(UART_HandleTypeDef* pUartHandle, DeviceCallback_t* pCallBack);
void DeviceDeinit(Device_t* pDevice);
void DeviceProcessing(Device_t* pDevice);


#endif
