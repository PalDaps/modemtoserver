#include "progress_server.h"

ServerStatus ProgressAuthSend(Modem_t* pModem)
{
  String_t Str = StrMakeFormCStr("{\"protocol\":\"service_information\",\"lsncmd\":\"00\",\"sourceDeviceId\":\"3-07-77-0-1\",\"payload\":\"$LSNCMD,00,00,00,00,00*37\"}");
  ModemSend(pModem, &Str);
  StrClear(&Str);
  return servOk;
}
