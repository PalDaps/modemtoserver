#ifndef PROGRESS_SERVER_H_
#define PROGRESS_SERVER_H_

#include "util_string.h"
#include "json_string.h"
#include "util_string.h"
#include "util_ring_buffer.h"
#include "modem.h"

typedef enum {
  servOk,
  servError,
  servRtkOn,
  servRtkOff,
  servPppOn,
  servPppOff
} ServerStatus;

ServerStatus ProgressAuthSend(Modem_t* pModem);
//ServerStatus ProgressAuthSend(char* pId, osMessageQueueId_t Queue);
//ServerStatus ProgressDataSend(osMessageQueueId_t Queue, String_t* pStr, FlowCtrl_t* pFlowCtrl);
//ServerStatus ServSockConnectCatch(String_t const* pStr);
//ServerStatus ServWebSockConnectCatch(String_t const* pStr);
//ServerStatus ServSockCloseCatch(String_t const* pStr);
//ServerStatus ServAuthCatch(FlowCtrl_t* pFlowCtrl, String_t const* pStr);
//ServerStatus ServFindClientId(String_t const* pStr, uint32_t* pId);
//
//void ProgressServAnalysis(FlowCtrl_t* pFlowCtrl, String_t const* pStr);
//
#endif /* PROGRESS_SERVER_H_ */
