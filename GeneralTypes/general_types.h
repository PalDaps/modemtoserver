#ifndef INC_GENERAL_TYPES_H_
#define INC_GENERAL_TYPES_H_

#define STATION_ID_shift    12
#define STATION_ID_length   12

typedef struct
{
	struct
	{
		uint8_t DeviceType;
		uint8_t Country;
		uint8_t Region;
		uint8_t SistemUser;
		uint8_t ZoneNumber;
		uint8_t StationNumber;
		uint8_t Sector;
	}Fields;
	char sId[STATION_ID_length];
}Id_t;

#endif /* INC_GENERAL_TYPES_H_*/
